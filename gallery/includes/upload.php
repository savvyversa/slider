<?php

$booImageUploaded = FALSE;
$UniqueFilename = "";
$ImageDimensionx = "";
$ImageDimensiony = "";

// if an upload attempt has been made
if (isset($_FILES['photoupload'])) {

   // if file has uploaded successfully continue otherwise print correct error   
   if ($_FILES['photoupload']['error'] != UPLOAD_ERR_OK) {

   	  print $_FILES['photoupload']['error'];
	  
      //unsuccessful print appropriate error
      if ($_FILES['photoupload']['error'] == UPLOAD_ERR_FORM_SIZE)
	     uploaderror("File upload failed: Your image file is too large. Maximum file size in upload form exceeded.");
      elseif ($_FILES['photoupload']['error'] == UPLOAD_ERR_INI_SIZE)	  
	    uploaderror("File upload failed: Your image file is too large. Maximum file size in server setting exceeded.");
      elseif ($_FILES['photoupload']['error'] == UPLOAD_ERR_NO_FILE)	
         uploaderror("No image uploaded.");
      elseif ($_FILES['photoupload']['error'] == UPLOAD_ERR_PARTIAL)
         uploaderror("File upload failed: Browser only send part of file.");
      elseif ($_FILES['photoupload']['error'] == UPLOAD_ERR_NO_TMP_DIR)
         uploaderror("File upload failed: upload_tmp_dir setting in php.ini file is incorrect.");		 
		 
   }
   else {
  
      //file upload successful so perform file security checks
	  
      //check file type
	  //print $_FILES['photoupload']['type'];
	  if ($_FILES['photoupload']['type'] != "image/jpeg" && $_FILES['photoupload']['type'] != "image/pjpeg") {
	  
         uploaderror("File not processed: File is not a jpeg/pjpeg image");

	  }
	  else {
	  
	     //check is an image
	     if (!getimagesize($_FILES['photoupload']['tmp_name'])) {

            uploaderror("File not processed: File is pretending to be an image");		 
		 
         }
         else {
		 
		    //check file is not executable
			$blacklist = array(".php", ".phtml", ".php3", ".php4", ".shtml", ".js", ".pl", ".py");
			
			$booExeflag = FALSE;
			foreach ($blacklist as $value) {
			   if (preg_match("/$value\$/i", $_FILES['photoupload']['name'])) {
			      $booExeflag = TRUE;
			   }
			}
			
			if ($booExeflag) {
			
			   uploaderror("File not processed: Loading executable files not allowed");
			
			}
			else
			{
		 
               // finally create and save fullsize and thumbnail size images in respective locations
			   
			   global $SETThumbnailPath;
			   global $SETFullsizePath;
               global $SETThumbnailWidth;
               global $SETThumbnailQuality;
               global $SETImageFilesChmod;
			   
			   
			   $strTempFilename = $_FILES['photoupload']['tmp_name'];
			   
			   // generate unique filename
			   $UniqueFilename = uniqid("p");
			   $UniqueFilename .= ".jpg";
			   
			   //create image resource from temp file to make smaller images with
               $imageresource  = imagecreatefromstring(file_get_contents($strTempFilename));
       
               //calculate resizing ratio	   
			   $FullsizeWidth    = imagesx($imageresource);
			   $ImageDimensionx  = $FullsizeWidth; 
               $FullsizeHeight   = imagesy($imageresource);
			   $ImageDimensiony  = $FullsizeHeight;
               $floatResizeRatio = $FullsizeHeight / $FullsizeWidth;

               //move copy of fullsize image to directory and rename
			   $FullsizeLocation = $SETFullsizePath . $UniqueFilename;
               move_uploaded_file($strTempFilename, $FullsizeLocation);			   

               //resize and publish to thumbnail folder
               $ThumbnailHeight = $SETThumbnailWidth * $floatResizeRatio;
               $imageresource3 = imagecreatetruecolor($SETThumbnailWidth, $ThumbnailHeight);
               imagecopyresampled($imageresource3, $imageresource, 0, 0, 0, 0, $SETThumbnailWidth, $ThumbnailHeight, imagesx($imageresource), imagesy($imageresource));
               $ThumbnailLocation = $SETThumbnailPath . $UniqueFilename;
               imagejpeg($imageresource3, $ThumbnailLocation, $SETThumbnailQuality);
               chmod($ThumbnailLocation, $SETImageFilesChmod);
			   
			   $booImageUploaded = TRUE;
			 
			 
			} // end check executable
		
		
         } //end check is an image
	  
	  
	  } //end check filetype
	  
   
   } // end file upload successful
   
   
} // end check for fileuploaded




function uploaderror($strError) {

   print("<div class=\"uploaderror\">$strError</div>");

}




?>
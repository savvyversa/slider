<?php

// ----------------------------------------------------------------------
//   File        : inc_categorylist.php
//   Description : display a list of category records
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




//define global variables
global $intPage;
global $intPageSize;
global $intRecordsFound;
global $SQLstmt;
global $intStartRecord;
global $intTotalPages;
global $strAdminState;



//accept variables

if (isset($_REQUEST['pn']))
   $intPage = $_REQUEST['pn'];
else
   $intPage = 1;

$intStartRecord = (($intPage*$intPageSize)-($intPageSize));




print("<div id=\"funcbar\"><div id=\"adduser\"><a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=newcategory$strAdminState\"><img src=\"images/addcategory.gif\" border=\"0\" alt=\"add category\"></a></div></div>");

ScreenHeading("Categories");




if ($_SESSION['AccessLevel'] == 'Super' || $_SESSION['AccessLevel'] == 'Administrator')
{

   //main list page logic

   print("<div id=\"categorylistcontainer\">\n");

   DisplaySearchForm();
   SqlPrep();
   RecordCount();
   GetPageTotal();

   if ($intRecordsFound > 0)
   {
      SqlForResults();
      SearchFeedback();

      print("<div id=\"statusbar\">\n");
      ResultsStatus();
      print("</div>\n"); // end statusbar div

      DisplayRecords();
      DisplayPagedNavigation($intTotalPages, $intPage);
	  print("<div id=\"bottomspacer\"></div>");
   }
   else
   {
      SearchFeedback();
      NoRecordsFound();
	  print("<div id=\"bottomspacer\"></div>");
   }

   print("</div>\n"); // end userlistcontainer div

}
else
{
   BasicMessage("You do not have access to this area");
}




// ----------------------------------------------------------------------
//   Function    : DisplaySearchForm
//   Description : display a search form including keyword search
//   Usage       : DisplaySearchForm()
//
// ----------------------------------------------------------------------

function DisplaySearchForm() {

   print("<div id=\"searchform\">\n");
   print("<table cellpadding=\"0\" cellspacing=\"0\" class=\"searchtable\">\n");
   print("<form action=\"" . $_SERVER['PHP_SELF'] ."\" method=\"POST\">\n");
   print("<tr><td class=\"searchformtext\">Search&nbsp;\n");
   print("<input type=\"hidden\" name=\"pageaction\" value=\"categorylist\">\n");
   print("<td><input type=\"text\" name=\"key\"></td>\n");
   print("<td><input type=\"image\" src=\"images/search.gif\" class=\"searchbutton\"></td>\n");
   print("</tr>\n");
   print("</form>\n");
   print("</table>\n");
   print("</div>\n");

}




// ----------------------------------------------------------------------
//   Function    : SqlPrep
//   Description : sets SQL variables to find number of records found
//                 and prepare to run main query
//   Usage       : SqlPrep()
//
// ----------------------------------------------------------------------

function SqlPrep() {

   global $SQLstmt, $SQLselect, $SQLfrom, $SQLwhere;
   global $strKey;
   global $SETDebug;

   //Build FROM part
   $SQLfrom =  " FROM igcategory";


   //Build WHERE part
   if ($strKey != "")
   {
      $SQLwhere .= " WHERE (categoryname LIKE '%$strKey%' OR categoryID LIKE '%$strKey%')"; 
   }

   //SQL to find number of records
   $SQLselect = "SELECT COUNT(*)";
   $SQLstmt = $SQLselect . $SQLfrom . $SQLwhere;

   if ($SETDebug == "ON")
   {
      print("<div class=\"debug\">$SQLstmt</div>");
   }

}




// ----------------------------------------------------------------------
//   Function    : RecordCount
//   Description : returns number of records found during search
//   Usage       : RecordCount($SQLstmt)
//   Parameters  : $SQLstmt - SQL for total query record count
//   Returns     : $intRecordsFound - how many records will be returned
//                 by query
//
// ----------------------------------------------------------------------

function RecordCount() {

   global $SQLstmt;
   global $intRecordsFound;

   $resultSet = dbaction($SQLstmt);
   $row = getrsrow($resultSet);
   $intRecordsFound = $row[0];

}




// ----------------------------------------------------------------------
//   Function    : GetPageTotal
//   Description : return the number of record result pages available
//                 using the total number of records found and the
//                 page size from the settings.php file
//   Usage       : GetPageTotal()
//   Returns     : intTotalPages - how many record result pages there are
//                 given the number of records found and the page size
//                 setting
//
// ----------------------------------------------------------------------

function GetPageTotal() {

   global $intPageSize;
   global $intRecordsFound;
   global $intTotalPages;

   if (fmod($intRecordsFound, $intPageSize) == 0)
   {
      $intTotalPages = $intRecordsFound/$intPageSize;      
   }
   else
   {
      $intTotalPages = ceil($intRecordsFound/$intPageSize);
   }

}




// ----------------------------------------------------------------------
//   Function    : SqlForResults
//   Description : creates the remainder of the sql parts required and 
//                 sets the actual record retrieval SQL query 
//   Usage       : SqlForResults()
//
// ----------------------------------------------------------------------

function SqlForResults() {

   global $SQLstmt, $SQLselect, $SQLfrom, $SQLwhere, $SQLorder;
   global $intPageSize, $intStartRecord;
   global $SETDebug;

   //SQL to retrieve all or a subset of user records
   $SQLselect = "SELECT categoryID, categoryname";
   $SQLorder = " ORDER BY categoryname ASC";
   $SQLlimit = " LIMIT $intStartRecord, $intPageSize";
   $SQLstmt = $SQLselect . $SQLfrom . $SQLwhere . $SQLorder . $SQLlimit;

   if ($SETDebug == "ON")
   {
      print("<div class=\"debug\">$SQLstmt</div>\n");
   }

}




// ----------------------------------------------------------------------
//   Function    : SearchFeedback
//   Description : displays the search criteria you specified for the
//                 record search
//   Usage       : SearchFeedback()
//
// ----------------------------------------------------------------------

function SearchFeedback() {

   global $strKey;

   if ($strKey != "") 
   {
      print("<div id=\"searchfeedback\">");
      print("You searched for: <span class=\"searchfeedbackbold\">" . stripslashes($strKey) . "</span>");
      print("</div>\n");
   }

}




// ----------------------------------------------------------------------
//   Function    : ResultsStatus
//   Description : shows status info on the number of records returned in
//                 a result list and which ones are currently shown
//   Usage       : ResultsStatus()
//
// ----------------------------------------------------------------------

function ResultsStatus() {

   global $intPage;
   global $intPageSize;
   global $intStartRecord;
   global $intRecordsFound;

   print("<div id=\"recordsfound\">");
   print("Showing results <span class=\"recordsfoundbold\">" . ($intStartRecord+1) . ""); 
   print("</span> - <span class=\"recordsfoundbold\">");

   if (($intPageSize*$intPage) < $intRecordsFound)
   {
      print($intPageSize*$intPage);
   }
   else
   {
      print($intRecordsFound);
   }            

   print(" </span> of <span class=\"recordsfoundbold\">$intRecordsFound</span>");
   print("</div>\n");

}




// ----------------------------------------------------------------------
//   Function    : DisplayRecords
//   Description : display the query results in a data table
//   Usage       : DisplayRecords()
//
// ----------------------------------------------------------------------

function DisplayRecords() {

   global $strAdminState;
   global $SQLstmt;

   $resultSet = dbaction($SQLstmt);


   //check to see if only 1 record is in this page to avoid state errors with deletions later on
   if (lastrec($resultSet)) 
   {
      $strLastRec = "yes";
   }


   print("<div id=\"userlist\">\n");

   print("<table class=\"datatable\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" width=\"600\">\n");
   print("  <tr>\n");
   print("    <td class=\"datatablehead\" align=\"center\">View</td>\n");
   print("    <td class=\"datatablehead\" align=\"center\">Edit</td>\n");
   print("    <td class=\"datatablehead\" align=\"center\">Delete</td>\n");
   print("    <td class=\"datatablehead\">Category Name</td>\n");
   print("  </tr>\n");


   $intAltRow = 1;

   while ($row = getrsrow($resultSet))
   {
      if ($intAltRow == 1)
         print("<tr class=\"datarowon\">\n");
      else
         print("<tr class=\"datarowoff\">\n");


      print("    <td width=\"38\" align=\"center\"><a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=viewcategory&id=" . $row["categoryID"] . "$strAdminState\"><img src=\"images/viewicon.gif\" border=\"0\"></a></td>\n");
      print("    <td width=\"38\" align=\"center\"><a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=editcategory&id=" . $row["categoryID"] . "$strAdminState\"><img src=\"images/editicon.gif\" border=\"0\"></a></td>\n");
      print("    <td width=\"38\" align=\"center\"><a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=deletecategory&id=" . $row["categoryID"] . "&name=" . urlencode($row["categoryname"]) . "$strAdminState");
      if (isset($strLastRec))
         print("&lastrec=true");      
      print("\"><img src=\"images/deleteicon.gif\" border=\"0\"></a></td>\n");
      print("    <td>" . $row["categoryname"] . "&nbsp;</td>\n");
      print("  </tr>\n");

      if ($intAltRow == 1)
         $intAltRow = 0;
      else
         $intAltRow = 1;

   }

   print("</table>\n");
   print("</div>\n"); // end userlist div

}




// ----------------------------------------------------------------------
//   Function    : NoRecordsFound
//   Description : print no records found message
//   Usage       : NoRecordsFound()
//
// ----------------------------------------------------------------------

function NoRecordsFound() {

   BasicMessage("No records found");

}


?>


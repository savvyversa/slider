<?php

// ----------------------------------------------------------------------
//   File        : categoryeditdropdown.php
//   Description : Display a dropdown form item with category records.
//                 It is used by the edit image function, automatically
//                 selecting the category in the list. 
//   Version     : 1.0
//   Created     : 1/3/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




$sqlStmt2 = "SELECT categoryID, categoryname FROM igcategory ORDER BY categoryname ASC";

$resultSet2 = dbaction($sqlStmt2);

print("<select name=\"categorynewedit\">\n");
print("  <option value=\"\">Choose...\n");

while ($row2 = getrsrow($resultSet2))
{
   print("  <option value=\"" . $row2["categoryID"] . "\"");
   if ($row2["categoryID"] == $row["categoryID"])
      print(" selected");
   print(">" . $row2["categoryname"] . "\n");
}   
   
print("</select>\n");




?>


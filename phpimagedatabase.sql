-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2016 at 05:26 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `phpimagedatabase`
--
CREATE DATABASE IF NOT EXISTS `phpimagedatabase` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `phpimagedatabase`;

-- --------------------------------------------------------

--
-- Table structure for table `igcategory`
--

DROP TABLE IF EXISTS `igcategory`;
CREATE TABLE IF NOT EXISTS `igcategory` (
  `categoryID` int(10) unsigned NOT NULL,
  `categoryname` varchar(100) DEFAULT NULL,
  `recordlastmodified` varchar(100) DEFAULT NULL,
  `recordlastmodifiedby` varchar(50) DEFAULT NULL,
  `recordcreated` varchar(100) DEFAULT NULL,
  `recordcreatedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `igimages`
--

DROP TABLE IF EXISTS `igimages`;
CREATE TABLE IF NOT EXISTS `igimages` (
  `imageID` int(10) unsigned NOT NULL,
  `publishdate` date DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `imagefile` varchar(100) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `keywordtags` varchar(255) DEFAULT NULL,
  `photographer` varchar(100) DEFAULT NULL,
  `imagedimensionx` varchar(20) DEFAULT NULL,
  `imagedimensiony` varchar(20) DEFAULT NULL,
  `categoryID` int(10) unsigned DEFAULT NULL,
  `recordstatus` varchar(20) DEFAULT NULL,
  `recordlastmodified` varchar(100) DEFAULT NULL,
  `recordlastmodifiedby` varchar(50) DEFAULT NULL,
  `recordcreated` varchar(100) DEFAULT NULL,
  `recordcreatedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iguser`
--

DROP TABLE IF EXISTS `iguser`;
CREATE TABLE IF NOT EXISTS `iguser` (
  `userID` int(10) unsigned NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `userfullname` varchar(50) DEFAULT NULL,
  `accesslevel` varchar(20) DEFAULT NULL,
  `recordstatus` varchar(20) DEFAULT NULL,
  `recordlastmodified` varchar(100) DEFAULT NULL,
  `recordlastmodifiedby` varchar(50) DEFAULT NULL,
  `recordcreated` varchar(100) DEFAULT NULL,
  `recordcreatedby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `igcategory`
--
ALTER TABLE `igcategory`
  ADD PRIMARY KEY (`categoryID`);

--
-- Indexes for table `igimages`
--
ALTER TABLE `igimages`
  ADD PRIMARY KEY (`imageID`);

--
-- Indexes for table `iguser`
--
ALTER TABLE `iguser`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `igcategory`
--
ALTER TABLE `igcategory`
  MODIFY `categoryID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `igimages`
--
ALTER TABLE `igimages`
  MODIFY `imageID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `iguser`
--
ALTER TABLE `iguser`
  MODIFY `userID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

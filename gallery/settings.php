<?php

// ----------------------------------------------------------------------
//   File        : settings.php
//   Description : application settings
//   Created     : 1/3/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




// General application settings

$SETApplicationName  = "PHP Image Database";
$SETSecurityID       = "php";
$SETSecurityOverride = "OFF";
$SETDebug            = "OFF";

$SETMaxFileSize        = 1000000;
$SETThumbnailPath      = "userimages/thumbs/";
$SETFullsizePath       = "userimages/fullsize/";
$SETThumbnailWidth     = 60;
$SETThumbnailQuality   = 100;
$SETImageFilesChmod    = 644;

$SETPublicGridColumns  = 5;
$SETAdminGridColumns   = 5;

$SETAdminYearStart   = 2010;
$SETAdminYearEnd     = 2020;


// mySQL Database settings

$MySQLservername   = "localhost";
$MySQLusername     = "root";
$MySQLpassword     = "";
$MySQLdatabasename = "phpimagedatabase";


//Public search record paging settings

$intPublicPageSize    = 20;
$intPublicChapterSize = 10;


//Administration record paging settings

$intImagesDisplaySize = 20;
$intPageSize    = 10;
$intChapterSize = 10;




?>
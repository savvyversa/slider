<?php

// ----------------------------------------------------------------------
//   File        : categorydropdownsearch.php
//   Description : display a dropdown form item with category records
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




$sqlStmt2 = "SELECT categoryID, categoryname FROM igcategory ORDER BY categoryname ASC";

$resultSet2 = dbaction($sqlStmt2);

print("<select name=\"category\">\n");
print("  <option value=\"\">Choose...\n");

while ($row2 = getrsrow($resultSet2))
{
   print("  <option value=\"" . $row2["categoryID"] . "\">" . $row2["categoryname"] . "\n");
}   
   
print("</select>\n");




?>


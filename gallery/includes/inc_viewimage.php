<?php

// ----------------------------------------------------------------------
//   File        : inc_viewimage.php
//   Description : displays full size image and data for an image record
//   Version     : 1.0
//   Created     : 1/3/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




global $SETFullsizePath;




screenheading("View image");




//get record from database
if (isset($_REQUEST['id']))
   (int)$intID = addslashes($_REQUEST['id']);
else
   $intID = null;

$SQLstmt = "SELECT * FROM igimages WHERE imageID = '$intID'";

$resultSet = dbaction($SQLstmt);
$row = getrsrow($resultSet);

   
 
   
//display view record

print("<div id=\"viewcontainer\">\n");

//format date
$tmpDate = $row["publishdate"];
$ytoken = strtok($tmpDate, "-");
$mtoken = strtok("-");
$dtoken = strtok("-");
   
@$tmpTS = mktime(0, 0, 0, $mtoken, $dtoken, $ytoken);
   
@$fmtDate = date('l, j F Y', $tmpTS);
   
print("<div id=\"viewpublishdate\">" . $fmtDate . "</div>\n");
print("<div id=\"viewtitle\">" . $row["title"] . "</div>\n");
print("<div id=\"viewimage\"><img src=\"" . $SETFullsizePath . $row["imagefile"] . "\"></div>\n");

print("</div>\n"); // end viewcontainer div

print("<table id=\"viewtable\">\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Caption</td>\n");
print("    <td class=\"viewtext\">" . $row["caption"] . "</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Keyword Tags</td>\n");
print("    <td class=\"viewtext\">" . $row["keywordtags"] . "</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Photographer</td>\n");
print("    <td class=\"viewtext\">" . $row["photographer"] . "</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">X Dimension</td>\n");
print("    <td class=\"viewtext\">" . $row["imagedimensionx"] . "</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Y Dimension</td>\n");
print("    <td class=\"viewtext\">" . $row["imagedimensiony"] . "</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Category</td>\n");
print("    <td class=\"viewtext\">");
if ($row["categoryID"] != ""  && $row["categoryID"] != 0)
{
   $SQLstmt2 = "SELECT categoryname FROM igcategory WHERE categoryID = '" . $row["categoryID"] . "'";
   $resultSet2 = dbaction($SQLstmt2);
   $row2 = getrsrow($resultSet2);
   print($row2["categoryname"]);
}
else
{
   print("Not assigned");
}
print("</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Publish</td>\n");
print("    <td class=\"viewtext\">");
if ($row["recordstatus"] == "")
{
   print("Inactive");
}
else
{
   print $row["recordstatus"];
}
print("</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Record Created</td>\n");
print("    <td class=\"viewtext\">" . $row["recordcreated"] . "</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Record Created By</td>\n");
print("    <td class=\"viewtext\">" . $row["recordcreatedby"] . "</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Record Last Modified</td>\n");
print("    <td class=\"viewtext\">" . $row["recordlastmodified"] . "</td>\n");
print("  </tr>\n");
print("  <tr>\n");
print("    <td class=\"viewhead\">Record Last Modified By</td>\n");
print("    <td class=\"viewtext\">" . $row["recordlastmodifiedby"] . "</td>\n");
print("  </tr>\n");
print("</table>\n");
print("<div id=\"bottomspacer\"></div>");




?>
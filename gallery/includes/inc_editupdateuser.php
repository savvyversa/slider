<?php

// ----------------------------------------------------------------------
//   File        : inc_editupdateuser.php
//   Description : displays a data populated form to update an existing
//                 user record. Submitting form validates data and 
//                 updates user record.  It is only available to Super 
//                 and Administrator users.
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("Edit user");




if ($_SESSION['AccessLevel'] == 'Super' || $_SESSION['AccessLevel'] == 'Administrator')
{

   //get subaction
   if (isset($_REQUEST['subaction']))
      $strSubAction = $_REQUEST['subaction'];
   else
      $strSubAction = NULL;
   
   //get record id
      if (isset($_REQUEST['id']))
      $intID = $_REQUEST['id'];
   else
      $intID = NULL;
	  
	  
   if ($strSubAction == "submit") {
   
      //accept data
	  if (isset($_REQUEST['username']))
         $strUsername = substr($_REQUEST['username'],0,50);
      else
         $strUsername = NULL;
		 
	  if (isset($_REQUEST['password']))
         $strPassword = substr($_REQUEST['password'],0,50);
      else
         $strPassword = NULL;	  
	  
	  if (isset($_REQUEST['userfullname']))
         $strUserFullName = substr($_REQUEST['userfullname'],0,50);
      else
         $strUserFullName = NULL;

	  if (isset($_REQUEST['accesslevel']))
         $strAccessLevel = $_REQUEST['accesslevel'];
      else
         $strAccessLevel = NULL;

	  if (isset($_REQUEST['userstatus']))
         $strUserStatus = $_REQUEST['userstatus'];
      else
         $strUserStatus = NULL;
		 
		 
	  //prepare data if magic quotes is off
      if (!get_magic_quotes_gpc()) {
		 $strPassword = addslashes($strPassword);
		 $strUserFullName = addslashes($strUserFullName);
		 $strAccessLevel = addslashes($strAccessLevel);
		 $strUserStatus = addslashes($strUserStatus);
	  }
	  
	  
      //validate form
      $booValid = TRUE;
	  $strValidateError = "";

  
	  //check userpassword field not empty
	  if ($strPassword == "") {
	     $booValid = FALSE;
		 $strValidateError = $strValidateError . "<li>Password cannot be empty<br>";
	  }
	  
	  //check userfullname field not empty
	  if ($strUserFullName == "") {
	     $booValid = FALSE;
		 $strValidateError = $strValidateError . "<li>User full name cannot be empty<br>";
	  }
	  	  
	  
	  //populate data
	  if ($booValid) {
	     UpdateNewUser($intID, $strPassword, $strUserFullName, $strAccessLevel, $strUserStatus);
	  }
	  else
	  {
	     DisplayEditUserForm($strSubAction, $strValidateError, $intID, $strUsername, $strPassword, $strUserFullName, $strAccessLevel, $strUserStatus);
	  }
	  
   }
   else {
      DisplayEditUserForm($strSubAction, "", $intID, "", "", "", "", "");
   }

}
else
{
   BasicMessage("You do not have access to this area");
}




function DisplayEditUserForm($strSubAction, $strValidateError, $intID, $strUsername, $strPassword, $strUserFullName, $strAccessLevel, $strUserStatus) {

   global $strFormAdminState;
   
   //get data to populateform
   if ($strSubAction == "")
   {
      $SQLstmt = "SELECT username, password, userfullname, accesslevel, recordstatus FROM iguser WHERE userID = '$intID'";
   
      $resultSet = dbaction($SQLstmt);
      $row = getrsrow($resultSet);
   }
   
   print("<table class=\"formtable\" width=\"572\" border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\n");
   print("<form action=\"" . $_SERVER['PHP_SELF'] . "\" method=\"post\">\n");
   print("<input type=\"hidden\" name=\"pageaction\" value=\"updateuser\">\n");
   print("<input type=\"hidden\" name=\"id\" value=\"$intID\">\n");   
   print $strFormAdminState;
   print("<input type=\"hidden\" name=\"subaction\" value=\"submit\">\n"); 
   
   //print errors if submit not valid
   if ($strValidateError != "" ) {
      print("  <tr>\n");
      print("    <td colspan=\"2\"><div id=\"formerror\">ERROR: Form not submitted due to invalid fields<br>" . $strValidateError . "</div></td>\n");
      print("  </tr>\n");
   }
   
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;Username</td>\n");
   print("    <td class=\"formtext\">");
   if ($strSubAction == "")
   {
      print($row["username"]);
   }
   else
   { 
      if ($strValidateError != "" )
         print($strUsername);   
   }
   if ($strSubAction == "")
   {
      print("<input type=\"hidden\" name=\"username\" value=\"" . $row["username"] . "\">");
   }
   else
   {
      print("<input type=\"hidden\" name=\"username\" value=\"" . stripslashes($strUsername) . "\">");
   }   
   print("</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;Password</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"password\" class=\"formitem\"");
   if ($strSubAction == "")
   {
      print(" value=\"" . $row["password"] . "\"");
   }
   else
   { 
      if ($strValidateError != "" )
         print(" value=\"" . stripslashes($strPassword) . "\"");   
   }
   print("></td>\n");
   print("  </tr>\n"); 
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;User Full Name</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"userfullname\" size=\"40\" class=\"formitem\"");
   if ($strSubAction == "")
   {
      print(" value=\"" . $row["userfullname"] . "\"");
   }
   else
   {
      if ($strValidateError != "" )
         print(" value=\"" . stripslashes($strUserFullName) . "\"");  
   }
   print("></td>\n");
   print("  </tr>\n");  
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;Access Level</td>\n");
   print("    <td class=\"formtext\"><select name=\"accesslevel\" class=\"formitem\">\n");
   print("        <option value=\"User\"");
   if ($strSubAction == "")
   {
      if ($row["accesslevel"] == "User")
         print(" selected");
   }
   else
   {
      if ($strValidateError != "" )
      {
         if ($strAccessLevel == "User")
	        print(" selected");
      }
   }
   print(">User\n");
   // Super users can create all user types, Administrator users are limited to creating the User type
   if ($_SESSION['AccessLevel'] == "Super")
   {
      print("        <option value=\"Administrator\"");
	  if ($strSubAction == "")
      {
         if ($row["accesslevel"] == "Administrator")
            print(" selected");
      }
      else
      {
	     if ($strValidateError != "" )
         {
            if ($strAccessLevel == "Administrator")
	           print(" selected");
         }
      }
	  print(">Administrator\n");
      print("        <option value=\"Super\"");
	  if ($strSubAction == "")
      {
         if ($row["accesslevel"] == "Super")
            print(" selected");
      }
      else
	  {
	     if ($strValidateError != "" )
         {
            if ($strAccessLevel == "Super")
	           print(" selected");
         }
      }
	  print(">Super\n");
   }
   print("</select>\n");
   print("    </td>\n");  
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;Active</td>\n");
   print("    <td class=\"formtext\"><input type=\"checkbox\" name=\"userstatus\" value=\"active\"");
   
   if ($strSubAction == "")
   {
      if ($row["recordstatus"] == "active")
         print(" checked");
   }
   else
   {
      if ($strValidateError != "")
      {
         if ($strUserStatus == "active")
            print(" checked");    
      } 
   }
   
   print("></td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formspacer\" colspan=\"2\"></td>\n");
   print("  </tr>\n");     
   print("  <tr>\n");
   print("    <td>&nbsp;</td>\n");
   print("    <td><input type=\"submit\" value=\"update user\"></td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("</form>\n");
   
}




function UpdateNewUser($intID, $strPassword, $strUserFullName, $strAccessLevel, $strUserStatus) {

   global $strAdminState;

   @ $dtDatetime = date("h:i A l F dS, Y");

   //generate sql statement
   $SQLstmt = "UPDATE iguser SET " .
   "password = '$strPassword', " .
   "userfullname = '$strUserFullName', " .
   "accesslevel = '$strAccessLevel', " .
   "recordstatus = '$strUserStatus', " .
   "recordlastmodifiedby = '" . addslashes($_SESSION['Username']) . "', " .
   "recordlastmodified = '$dtDatetime' " .
   "WHERE userID = '$intID'";
   
   // execute statement
   dbaction($SQLstmt); 

   ConfirmMessage("User record updated", "" . $_SERVER['PHP_SELF'] . "?pageaction=userlist$strAdminState");
  	  
}



?>

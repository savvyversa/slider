create database phpimagedatabase;

use phpimagedatabase;

create table igimages
(imageID int unsigned not null auto_increment primary key,
publishdate date,
title varchar(255),
imagefile varchar(100),
caption varchar(255),
keywordtags varchar(255),
photographer varchar(100),
imagedimensionx varchar(20),
imagedimensiony varchar(20),
categoryID int unsigned,
recordstatus varchar(20),
recordlastmodified varchar(100),
recordlastmodifiedby varchar(50),
recordcreated varchar(100),
recordcreatedby varchar(50)
);

create table igcategory
(categoryID int unsigned not null auto_increment primary key,
categoryname varchar(100),
recordlastmodified varchar(100),
recordlastmodifiedby varchar(50),
recordcreated varchar(100),
recordcreatedby varchar(50)
);

create table iguser
(userID int unsigned not null auto_increment primary key,
username varchar(50),
password varchar(50),
userfullname varchar(50),
accesslevel varchar(20),
recordstatus varchar(20),
recordlastmodified varchar(100),
recordlastmodifiedby varchar(50),
recordcreated varchar(100),
recordcreatedby varchar(50)
);


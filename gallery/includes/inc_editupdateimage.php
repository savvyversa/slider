<?php

// ----------------------------------------------------------------------
//   File        : inc_editupdateimage.php
//   Description : displays a data populated form to update an existing
//                 image record. Submitting form updates release record
//                 and may update image.
//   Version     : 1.0
//   Created     : 1/3/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("Edit image");




//get subaction
if (isset($_REQUEST['subaction']))
   $strSubAction = $_REQUEST['subaction'];
else
   $strSubAction = NULL;

//get record id
if (isset($_REQUEST['id']))
   $intID = $_REQUEST['id'];
else
   $intID = NULL;   

   
if ($strSubAction == "submit") {
   
   //accept data
   if (isset($_REQUEST['imagefileradio']))
      $strImageFileRadio = $_REQUEST['imagefileradio'];
   else
      $strImageFileRadio = "";
   
   if (isset($_REQUEST['imagefile']))
      $imagefile = $_REQUEST['imagefile'];
   else
      $imagefile = "";
	  
   if (isset($_REQUEST['publishdateradio']))
      $strPublishDateRadio = $_REQUEST['publishdateradio'];
   else
      $strPublishDateRadio = "";

   if (isset($_REQUEST['publishdate']))
      $dtPublishDate = $_REQUEST['publishdate'];
   else
      $dtPublishDate = "";
	  
   if (isset($_REQUEST['dateday']))
      $strDateDay = $_REQUEST['dateday'];
   else
      $strDateDay = "";

   if ($strDateDay == "")
   {
      $strDateDay = "00";
   }
   
   if (isset($_REQUEST['datemonthnewedit']))
      $strDateMonth = $_REQUEST['datemonthnewedit'];
   else
      $strDateMonth = "";

   if ($strDateMonth == "")
   {
      $strDateMonth = "00";
   }
   
   if (isset($_REQUEST['dateyearnewedit']))
      $strDateYear = $_REQUEST['dateyearnewedit'];
   else
      $strDateYear = "";	  

   if ($strDateYear == "")
   {
      $strDateYear = "0000";
   }
   
   if (isset($_REQUEST['title']))
      $strTitle = substr($_REQUEST['title'],0,255);
   else
      $strTitle = NULL;
	  
   if (isset($_REQUEST['caption']))
      $strCaption = substr($_REQUEST['caption'],0,255);
   else
      $strCaption = NULL;	  

   if (isset($_REQUEST['keywordtags']))
      $strKeywordTags = substr($_REQUEST['keywordtags'],0,255);
   else
      $strKeywordTags = NULL;

   if (isset($_REQUEST['photographer']))
      $strPhotographer = substr($_REQUEST['photographer'],0,100);
   else
      $strPhotographer = NULL;

   if (isset($_REQUEST['xdimension']))
      $intXDimension = $_REQUEST['xdimension'];
   else
      $intXDimension = "";
	  
   if (isset($_REQUEST['ydimension']))
      $intYDimension = $_REQUEST['ydimension'];
   else
      $intYDimension = "";	  
	  
   if (isset($_REQUEST['categorynewedit']))
      $intCategory = $_REQUEST['categorynewedit'];
   else
      $intCategory = NULL;
	  
   if ($intCategory == "")
   {
      $intCategory = 0;
   }

   if (isset($_REQUEST['publish']))
      $strPublish = $_REQUEST['publish'];
   else
      $strPublish = NULL;	  
		 
		 
	//prepare data if magic quotes is off
    if (!get_magic_quotes_gpc()) {
	  $strDateDay = addslashes($strDateDay);
	  $strDateMonth = addslashes($strDateMonth);
	  $strDateYear = addslashes($strDateYear);
	  $strTitle = addslashes($strTitle);
	  $strCaption = addslashes($strCaption);
	  $strKeywordTags = addslashes($strKeywordTags);
	  $strPhotographer = addslashes($strPhotographer);	  
	  $intCategory = addslashes($intCategory);
	  $strPublish = addslashes($strPublish);	  
	}
	  
	  
   UpdateImage($intID, $strImageFileRadio, $imagefile, $strPublishDateRadio, $dtPublishDate, $strDateDay, $strDateMonth, $strDateYear, $strTitle, $strCaption, $strKeywordTags, $strPhotographer, $intXDimension, $intYDimension, $intCategory, $strPublish);
   
}
else
{
   DisplayEditImageForm($strSubAction, $intID);
}




function DisplayEditImageForm($strSubAction, $intID) {

   global $strFormAdminState;
   global $SETThumbnailPath;
   global $SETMaxFileSize;

   //get data to populateform
   if ($strSubAction == "")
   {
      $SQLstmt = "SELECT * FROM igimages WHERE imageID = '$intID'";
   
      $resultSet = dbaction($SQLstmt);
      $row = getrsrow($resultSet);
   }
   
   print("<table class=\"formtable\" width=\"572\" border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\n");
   print("<form enctype=\"multipart/form-data\" action=\"" . $_SERVER['PHP_SELF'] . "\" name=\"editform\" method=\"post\">\n");
   print("<input type=\"hidden\" name=\"pageaction\" value=\"updateimage\">\n");
   print $strFormAdminState;
   print("<input type=\"hidden\" name=\"subaction\" value=\"submit\">\n");
   print("<input type=\"hidden\" name=\"id\" value=\"$intID\">\n"); 
   print("<input type=\"hidden\" name=\"publishdate\" value=\"" . $row["publishdate"] . "\">\n");   
   print("<input type=\"hidden\" name=\"imagefile\" value=\"" . $row["imagefile"] . "\">\n");
   print("<input type=\"hidden\" name=\"xdimension\" value=\"" . $row["imagedimensionx"] . "\">\n");
   print("<input type=\"hidden\" name=\"ydimension\" value=\"" . $row["imagedimensiony"] . "\">\n");   
   print("<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"$SETMaxFileSize\">\n");   
   
   print("  <tr>\n");
   print("    <td class=\"formhead\">Image File</td>\n");
   print("    <td class=\"formtext\">");
   print("<input type=\"radio\" name=\"imagefileradio\" value=\"existing\" checked> Use existing: <img src=\"" . $SETThumbnailPath . $row["imagefile"] . "\"> " . $row["imagefile"] . "<br>\n");
   print("<input type=\"radio\" name=\"imagefileradio\" value=\"usenew\"> Change: <input name=\"photoupload\" type=\"file\" size=\"50\">");
   print("</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">Publish Date</td>\n");
   print("    <td class=\"formtext\">");
   print("<input type=\"radio\" name=\"publishdateradio\" value=\"existing\" checked> Use existing date: " . $row["publishdate"] . "<br>\n");
   print("<input type=\"radio\" name=\"publishdateradio\" value=\"usenew\"> Change date: ");
   include("includes/daypart.php");
   include("includes/monthpart.php");
   include("includes/yearpart.php");
   print("</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">Title</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"title\" size=\"50\" value=\"" . $row["title"] . "\" class=\"formitem\"></td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">Caption</td>\n");
   print("    <td class=\"formtext\"><textarea name=\"caption\" rows=\"4\" cols=\"50\" class=\"formitem\" onkeyup=\"CheckFieldLength(caption, 'charcount1', 'remaining1', 255);\" onkeydown=\"CheckFieldLength(caption, 'charcount1', 'remaining1', 255);\" onmouseout=\"CheckFieldLength(caption, 'charcount1', 'remaining1',255);\" onmouseover=\"CheckFieldLength(caption, 'charcount1', 'remaining1',255);\">" . $row["caption"] . "</textarea><br>\n");
   print("<small><span id=\"charcount1\">0</span> characters entered.   |   <span id=\"remaining1\">255</span> characters remaining.</small>\n");
   print("    </td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">Keyword Tags</td>\n");
   print("    <td class=\"formtext\"><textarea name=\"keywordtags\" rows=\"4\" cols=\"50\" class=\"formitem\" onkeyup=\"CheckFieldLength(keywordtags, 'charcount2', 'remaining2', 255);\" onkeydown=\"CheckFieldLength(keywordtags, 'charcount2', 'remaining2', 255);\" onmouseout=\"CheckFieldLength(keywordtags, 'charcount2', 'remaining2',255);\" onmouseover=\"CheckFieldLength(keywordtags, 'charcount2', 'remaining2',255);\">" . $row["keywordtags"] . "</textarea><br>\n");
   print("<small><span id=\"charcount2\">0</span> characters entered.   |   <span id=\"remaining2\">255</span> characters remaining.</small><br>\n");
   print("(separate keywords with commas)\n");
   print("    </td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">Photographer</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"photographer\" size=\"50\" value=\"" . $row["photographer"] . "\" class=\"formitem\"></td>\n");
   print("  </tr>\n");   
   print("  <tr>\n");
   print("    <td class=\"formhead\">Category</td>\n");
   print("    <td class=\"formtext\">");
   include("includes/categoryeditdropdown.php");
   print("</td>\n");
   print("  </tr>\n");   
   print("  <tr>\n");
   print("    <td class=\"formhead\">Publish</td>\n");
   print("    <td class=\"formtext\"><input type=\"checkbox\" name=\"publish\" value=\"active\" class=\"formitem\"");
   if ($row["recordstatus"] == "active")
   print(" checked");
   print("></td>\n");
   print("  </tr>\n");   
   print("  <tr>\n");
   print("    <td class=\"formspacer\" colspan=\"2\"></td>\n");
   print("  </tr>\n");     
   print("  <tr>\n");
   print("    <td>&nbsp;</td>\n");
   print("    <td><input type=\"submit\" value=\"update image\"></td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("</form>\n");
   print("<div id=\"bottomspacer\"></div>");
}




function UpdateImage($intID, $strImageFileRadio, $imagefile, $strPublishDateRadio, $dtPublishDate, $strDateDay, $strDateMonth, $strDateYear, $strTitle, $strCaption, $strKeywordTags, $strPhotographer, $intXDimension, $intYDimension, $intCategory, $strPublish) {

   global $strAdminState;

   @ $dtDatetime = date("h:i A l F dS, Y");

   if ($strPublishDateRadio == "existing")
      $dtDate = $dtPublishDate;
   else
      if ($strPublishDateRadio == "usenew")
         $dtDate = $strDateYear . "-" . $strDateMonth . "-" . $strDateDay;

   if ($strImageFileRadio == "existing")
   {   
      $strImageFile = $imagefile;
	  $imagedimensionx = $intXDimension;
	  $imagedimensiony = $intYDimension;
   }
   else if ($strImageFileRadio == "usenew")
   {
      include("upload.php");
	  $strImageFile = $UniqueFilename;
	  $imagedimensionx = $ImageDimensionx;
	  $imagedimensiony = $ImageDimensiony;
   }
		 
   //generate sql statement
   $SQLStmt = "UPDATE igimages SET " .
   "publishdate = '$dtDate', " .
   "title = '$strTitle', " .
   "imagefile = '$strImageFile', " .
   "caption = '$strCaption', " .   
   "keywordtags = '$strKeywordTags', " .
   "photographer = '$strPhotographer', " .
   "imagedimensionx = '$imagedimensionx', " .
   "imagedimensiony = '$imagedimensiony', " .
   "categoryID = '$intCategory', " .
   "recordstatus = '$strPublish', " .
   "recordlastmodified = '$dtDatetime', " .
   "recordlastmodifiedby = '" . addslashes($_SESSION['Username']) . "' " .    
   "WHERE imageID = '$intID'";
   
   // execute statement
   dbaction($SQLStmt); 

   ConfirmMessage("Image record updated", "" . $_SERVER['PHP_SELF'] . "?pageaction=images$strAdminState");
  	  
}




?>
// fieldname, warningname, remainingname, maxchars
function CheckFieldLength(fn,wn,rn,mc) {
  var len = fn.value.length;
  if (len > mc) {
    fn.value = fn.value.substring(0,mc);
    len = mc;
  }
  document.getElementById(wn).innerHTML = len;
  document.getElementById(rn).innerHTML = mc - len;
}


function CheckFieldLengthsOnEdit() {

var textlength;
var remaininglength;

textlength = document.editform.caption.value.length;
remaininglength = 255 - textlength;

charcount1.innerHTML = textlength;
remaining1.innerHTML = remaininglength;

var textlength2;
var remaininglength2;

textlength2 = document.editform.keywordtags.value.length;
remaininglength2 = 255 - textlength2;

charcount2.innerHTML = textlength2;
remaining2.innerHTML = remaininglength2;
}
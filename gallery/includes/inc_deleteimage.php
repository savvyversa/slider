<?php

// ----------------------------------------------------------------------
//   File        : inc_deleteimage.php
//   Description : displays options to delete image record and related
//                 files
//   Version     : 1.0
//   Created     : 1/3/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("Delete image");

	  


global $strAdminState;
global $SETThumbnailPath;
global $SETFullsizePath;



if (isset($_REQUEST['id']))
   (int)$intID = addslashes($_REQUEST['id']);
else
   $intID = null;

if(isset($_REQUEST['name']))
   $strName = $_REQUEST['name'];
else
   $strName = null;

if (isset($_REQUEST['subaction']))
   $strSubAction = $_REQUEST['subaction'];
else
   $strSubAction = NULL;

	  
if ($strSubAction == "confirm")
{

   // delete related files
   $SQLstmt = "SELECT imagefile FROM igimages WHERE imageID = '$intID'";
   $recordSet = dbaction($SQLstmt);
   $row = getrsrow($recordSet);
   $strImageFile = $row["imagefile"];

   $strFullsizeFile = $SETFullsizePath . $strImageFile;
   $strThumbnailFile = $SETThumbnailPath . $strImageFile;
   
   //print("$strFullsizeFile<br>");
   //print("$strThumbnailFile");
   
   unlink($strFullsizeFile);
   unlink($strThumbnailFile);
      
   
   // delete record
   $SQLstmt = "DELETE FROM igimages WHERE imageID = '$intID'";
   dbaction($SQLstmt);
   

   ConfirmMessage("Image $strName deleted", "" . $_SERVER['PHP_SELF'] . "?pageaction=images$strAdminState");
}
   else
   {
      ConfirmCancelMessage("You have chosen to delete image $strName", "" . $_SERVER['PHP_SELF'] . "?pageaction=deleteimage&subaction=confirm&id=$intID&name=" . urlencode($strName) . "$strAdminStateLastRec", "" . $_SERVER['PHP_SELF'] . "?pageaction=images$strAdminState");	  
   }




?>
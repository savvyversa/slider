<?php

// ----------------------------------------------------------------------
//   File        : inc_viewcategory.php
//   Description : displays data for a category record
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("View category");




if ($_SESSION['AccessLevel'] == 'Super' || $_SESSION['AccessLevel'] == 'Administrator')
{

   //get record from database
   if (isset($_REQUEST['id']))
      (int)$intID = addslashes($_REQUEST['id']);
   else
      $intID = null;

   $SQLstmt = "SELECT * FROM igcategory WHERE categoryID = '$intID'";

   $resultSet = dbaction($SQLstmt);
   $row = getrsrow($resultSet);

   
   
   
   //display view record
   print("<table id=\"viewtable\">\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Category Name</td>\n");
   print("    <td class=\"viewtext\">" . $row["categoryname"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Record Created</td>\n");
   print("    <td class=\"viewtext\">" . $row["recordcreated"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Record Created By</td>\n");
   print("    <td class=\"viewtext\">" . $row["recordcreatedby"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Record Last Modified</td>\n");
   print("    <td class=\"viewtext\">" . $row["recordlastmodified"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Record Last Modified By</td>\n");
   print("    <td class=\"viewtext\">" . $row["recordlastmodifiedby"] . "</td>\n");
   print("  </tr>\n");
   print("</table>\n");

}
else
{
   BasicMessage("You do not have access to this area");
}




?>
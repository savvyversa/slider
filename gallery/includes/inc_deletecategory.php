<?php

// ----------------------------------------------------------------------
//   File        : inc_deletecategory.php
//   Description : displays options to delete category record
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("Delete category");

	  


global $strAdminState;




if ($_SESSION['AccessLevel'] == 'Super' || $_SESSION['AccessLevel'] == 'Administrator')
{

   if (isset($_REQUEST['id']))
      (int)$intID = addslashes($_REQUEST['id']);
   else
      $intID = null;

   if(isset($_REQUEST['name']))
      $strName = $_REQUEST['name'];
   else
      $strName = null;

   if (isset($_REQUEST['subaction']))
      $strSubAction = $_REQUEST['subaction'];
   else
      $strSubAction = NULL;

	  
   if ($strSubAction == "confirm")
   {   
      //  delete record
      $SQLstmt = "DELETE FROM igcategory WHERE categoryID = '$intID'";
      dbaction($SQLstmt);
	  ConfirmMessage("Category $strName deleted", "" . $_SERVER['PHP_SELF'] . "?pageaction=categorylist$strAdminState");
   }
   else
   {
      ConfirmCancelMessage("You have chosen to delete category $strName", "" . $_SERVER['PHP_SELF'] . "?pageaction=deletecategory&subaction=confirm&id=$intID&name=" . urlencode($strName) . "$strAdminStateLastRec", "" . $_SERVER['PHP_SELF'] . "?pageaction=categorylist$strAdminState");	  
   }


}
else
{
   BasicMessage("You do not have access to this area");
}



?>
<?php

// ----------------------------------------------------------------------
//   File        : inc_deleteuser.php
//   Description : displays options to delete user record
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("Delete user");

	  


global $strAdminState;




if ($_SESSION['AccessLevel'] == 'Super' || $_SESSION['AccessLevel'] == 'Administrator')
{

   if (isset($_REQUEST['id']))
      (int)$intID = addslashes($_REQUEST['id']);
   else
      $intID = null;

   if(isset($_REQUEST['name']))
      $strName = $_REQUEST['name'];
   else
      $strName = null;

   if (isset($_REQUEST['subaction']))
      $strSubAction = $_REQUEST['subaction'];
   else
      $strSubAction = NULL;

	  
   if ($strSubAction == "confirm")
   {   
      //  delete record
      $SQLstmt = "DELETE FROM iguser WHERE userID = '$intID'";
      dbaction($SQLstmt);
	  ConfirmMessage("User $strName deleted", "" . $_SERVER['PHP_SELF'] . "?pageaction=userlist$strAdminState");
   }
   else
   {
      ConfirmCancelMessage("You have chosen to delete user $strName", "" . $_SERVER['PHP_SELF'] . "?pageaction=deleteuser&subaction=confirm&id=$intID&name=" . urlencode($strName) . "$strAdminStateLastRec", "" . $_SERVER['PHP_SELF'] . "?pageaction=userlist$strAdminState");	  
   }


}
else
{
   BasicMessage("You do not have access to this area");
}



?>
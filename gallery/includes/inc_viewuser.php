<?php

// ----------------------------------------------------------------------
//   File        : inc_viewuser.php
//   Description : displays data for a user record
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("View user");




if ($_SESSION['AccessLevel'] == 'Super' || $_SESSION['AccessLevel'] == 'Administrator')
{

   //get record from database
   if (isset($_REQUEST['id']))
      (int)$intID = addslashes($_REQUEST['id']);
   else
      $intID = null;

   $SQLstmt = "SELECT * FROM iguser WHERE userID = '$intID'";

   $resultSet = dbaction($SQLstmt);
   $row = getrsrow($resultSet);

   
   
   
   //display view record
   print("<table id=\"viewtable\">\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Username</td>\n");
   print("    <td class=\"viewtext\">" . $row["username"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">User Full Name</td>\n");
   print("    <td class=\"viewtext\">" . $row["userfullname"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Password</td>\n");
   print("    <td class=\"viewtext\">" . $row["password"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Access Level</td>\n"); 
   print("    <td class=\"viewtext\">" . $row["accesslevel"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">User Status</td>\n");
   print("    <td class=\"viewtext\">");
   if ($row["recordstatus"] == "")
   {
      print("Inactive");
   }
   else
   {
      print $row["recordstatus"];
   }
   print("</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Record Created</td>\n");
   print("    <td class=\"viewtext\">" . $row["recordcreated"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Record Created By</td>\n");
   print("    <td class=\"viewtext\">" . $row["recordcreatedby"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Record Last Modified</td>\n");
   print("    <td class=\"viewtext\">" . $row["recordlastmodified"] . "</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"viewhead\">Record Last Modified By</td>\n");
   print("    <td class=\"viewtext\">" . $row["recordlastmodifiedby"] . "</td>\n");
   print("  </tr>\n");
   print("</table>\n");

}
else
{
   BasicMessage("You do not have access to this area");
}




?>
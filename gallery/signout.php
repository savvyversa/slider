<?php

// ----------------------------------------------------------------------
//   File        : signout.php
//   Description : clear all security related session variables and
//                 display signed out message
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------

session_start();

unset($_SESSION['securityID']);
unset($_SESSION['username']);
unset($_SESSION['userfullname']);
unset($_SESSION['accesslevel']);
unset($_SESSION['area']);
	  
session_destroy();	  
	  
print("<!DOCTYPE html>\n");
print("<html>\n");
print("<head>\n");
print("  <title>Signout</title>\n");
print("  <link rel=stylesheet href=\"administration.css\" type=\"text/css\">\n");	  
print("</head>\n");
print("<body>\n");

print("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
print("  <tr>\n");
print("    <td align=\"center\">\n");

print("<div id=\"signoutmsg\">You are signing out.  Please close this window to exit.</div>\n");

print("</td>\n");
print("  </tr>\n");
print("</table>\n");

print("</body>\n");
print("</html>\n");


?>
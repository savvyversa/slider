<?php

// ----------------------------------------------------------------------
//   File        : inc_security.php
//   Description : prevents external operation of admin include files
//   Version     : 1.0
//   Created     : 1/12/2014
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




global $SETSecurityID;




if (!isset($_SESSION['SecurityID']) || $_SESSION['SecurityID'] != $SETSecurityID)
   die();

   
   
   
?>
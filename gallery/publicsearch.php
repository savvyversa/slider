<?php

// ----------------------------------------------------------------------
//   File        : publicsearch.php
//   Description : search and display grid of images
//   Version     : 1.0
//   Created     : 1/3/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("settings.php");
include("databasewrapper.php");




$strPublicState = GetPublicState(0);
$strNavPublicState = GetPublicState(1);




//accept parameters
if (isset($_REQUEST['stype']))
   $strSearchType = $_REQUEST['stype'];
else
   $strSearchType = NULL;
   
if (isset($_REQUEST['key']))
   $strKey = $_REQUEST['key'];
else
   $strKey = NULL;

if (isset($_REQUEST['datemonth']))
   (int)$dtMonth = $_REQUEST['datemonth'];
else
   $dtMonth = NULL;
   
if (isset($_REQUEST['dateyear']))
   (int)$dtYear = $_REQUEST['dateyear'];
else
   $dtYear = NULL;   

if (isset($_REQUEST['category']))
   (int)$intCategory = $_REQUEST['category'];
else
   $intCategory = NULL;   

if (isset($_REQUEST['imageid']))
   (int)$intImageID = $_REQUEST['imageid'];
else
   $intImageID = NULL;

if (isset($_REQUEST['pn']))
   $intPage = $_REQUEST['pn'];
else
   $intPage = 1;

$intStartRecord = (($intPage*$intPublicPageSize)-($intPublicPageSize));

   
//prepare data if magic quotes is off
if (!get_magic_quotes_gpc()) {
   $strKey = addslashes($strKey);
   $dtMonth = addslashes($dtMonth);
   $dtYear = addslashes($dtYear);
   $intCategory = addslashes($intCategory);
   $intImageID = addslashes($intImageID);   
}	  




print("<div id=\"igcontainer\">\n");

if ($strSearchType == "viewimage")
{
   SearchBar();
   ViewImage($intImageID);
}
else
{
   SqlPrep(); 
   $intRecordsFound = RecordCount($SQLstmt);
   $intTotalPages = GetPageTotal();
      
   if ($intRecordsFound > 0)
   {
      SqlForResults();
      SearchBar();
      SearchFeedback();
      ResultsStatus();
      ImageGrid();
	  DisplayPagedNavigation($intTotalPages, $intPage);
	  print("<div id=\"bottomspacer\"></div>");
   }
   else
   {
      SearchBar();
      SearchFeedback();
      NoRecordsFound();
	  print("<div id=\"bottomspacer\"></div>");
   } 
   
}
   
print("</div>\n"); // end igcontainer div




// ----------------------------------------------------------------------
//   Function    : SearchBar
//   Description : Displays form with search options
//   Usage       : SearchBar()
//
// ----------------------------------------------------------------------

function SearchBar()
{

   print("<table class=\"searchtable\" cellpadding=\"0\" cellspacing=\"0\">");
   print("<form action=\"". $_SERVER['PHP_SELF'] . "\" method=\"post\">");
   print("  <tr>\n");
   print("    <td>\n");
   
   print("<div id=\"searchtitle\">Search images</div>\n");
   
   print("<div id=\"searchfields\">\n");
   print("<table>\n");
   print("  <tr>\n");
   print("    <td class=\"searchfieldshead\">Date</td>\n");
   print("    <td></td>\n");
   print("    <td class=\"searchfieldshead\">Keyword</td>\n");
   print("    <td class=\"searchfieldshead\">Category</td>\n");
   print("    <td></td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td>\n");
   include("includes/monthpartsearch.php");
   print("    </td>\n");
   print("    <td>\n");
   include("includes/yearpartsearch.php");   
   print("    </td>\n");
   print("    <td><input type=\"text\" name=\"key\"></td>\n");
   print("    <td>\n");
   include("includes/categorydropdownsearch.php");
   print("    </td>\n");
   print("    <td><input type=\"submit\" value=\"go\"></td>\n");
   print("  </tr>\n");
  
   print("</table>\n");
   print("</div>\n"); //end searchfields div
   
   print("</td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("</form>\n");
}




// ----------------------------------------------------------------------
//   Function    : ViewImage
//   Description : displays details of an image record
//   Usage       : ViewImage($intImageID)
//   Parameters  : $intImageID -  ID number of image record
//
// ----------------------------------------------------------------------

function ViewImage($intImageID) {

   global $SETFullsizePath;

   //build SQL statement
   $SQLstmt = "SELECT * FROM igimages WHERE imageID = '$intImageID'";
   
   $resultSet = dbaction($SQLstmt);
   $row = getrsrow($resultSet);
   
   print("<div id=\"viewresult\">\n");
   
   print("<div id=\"backcontainer\">\n");
   backlink();
   print("</div>\n"); // end backcontainer div
   
   print("<div id=\"viewtitle\">" . $row["title"] . "</div>\n");
   
   //format date
   $tmpDate = $row["publishdate"];
   $ytoken = strtok($tmpDate, "-");
   $mtoken = strtok("-");
   $dtoken = strtok("-");
   
   @$tmpTS = mktime(0, 0, 0, $mtoken, $dtoken, $ytoken);
   
   @$fmtDate = date('j F Y', $tmpTS);
   
   print("<div id=\"viewdate\">" . $fmtDate . "</div>\n");

   print("<div id=\"viewphotographer\"><span class=\"viewphotographerhead\">Photographer: </span>" . $row["photographer"] . "</div>\n");
   
   print("<div id=\"viewfullsizeimage\"><img src=\"" . $SETFullsizePath . $row["imagefile"] . "\"></div>");
   
   print("<div id=\"viewcaption\">" . $row["caption"] . "</div>\n");  
   
   print("<div id=\"viewkeywordtags\">\n");
   print("Tags: ");
   $strKeywordTags = $row["keywordtags"];
   taglinks($strKeywordTags);
   print("</div>\n"); // end viewresult
   
}




// ----------------------------------------------------------------------
//   Function    : backlink()
//   Description : displays back to images link on view page
//   Usage       : backlink()
//
// ----------------------------------------------------------------------

function backlink() {

   global $strPublicState;

   print("<div id=\"backlink\"><a href=\"index.php?search=true$strPublicState\">Back to images</a></div>\n");

}




// ----------------------------------------------------------------------
//   Function    : taglinks()
//   Description : processes an images' keywordtags field to display
//                 tag links
//   Usage       : taglinks($keywordtags)
//
// ----------------------------------------------------------------------

function taglinks($keywordtags) {

   print("&nbsp;&nbsp;");

   //tokenized based on commas
   for ($token = strtok($keywordtags, ",");$token != "";$token = strtok(","))
   {
      print("<a href=\"" . $_SERVER['PHP_SELF'] . "?search=true&key=" . urlencode(trim($token)) . "\">" . trim($token) . "</a> ");
      print str_repeat("&nbsp;",2);
   }

}




// ----------------------------------------------------------------------
//   Function    : ImageGrid
//   Description : displays a table of image record results as a grid
//   Usage       : ImageGrid()
//
// ----------------------------------------------------------------------

function ImageGrid() {

   global $strPublicState;
   global $SQLstmt;
   global $SETThumbnailPath;
   global $SETPublicGridColumns;

   
   $resultSet = dbaction($SQLstmt); 
   
   print("<div id=\"imagegrid\">\n");
   
   print("  <table id=\"imagegridtable\" cellspacing=\"10\">\n");

   
   $intColumnCount = 0;
   
   
   while ($row = getrsrow($resultSet)) {
   
      if ($intColumnCount == 0)
	  {
         print("    <tr>\n");
	  }
   
      print("<td class=\"gridcell\" valign=\"top\">\n");
	  print("<a href=\"" . $_SERVER['PHP_SELF'] . "?stype=viewimage&imageid=" . $row["imageID"] . "$strPublicState\"><img src=\"" . $SETThumbnailPath . $row["imagefile"] . "\" border=\"0\"></a>\n");
	  
	  $tmpDate = $row["publishdate"];
      $ytoken = strtok($tmpDate, "-");
      $mtoken = strtok("-");
      $dtoken = strtok("-");
   
      @$tmpTS = mktime(0, 0, 0, $mtoken, $dtoken, $ytoken);
   
      @$fmtDate = date('j F Y', $tmpTS);
   
	  print("<div class=\"gridpublishdate\">" . $fmtDate . "</div>\n");
	  print("<div class=\"gridtitle\">" . $row["title"] . "</div>\n");
	  print("<div class=\"gridphotographer\">" . $row["photographer"] . "</div>\n");
	  print("<div class=\"gridfilename\">" . $row["imagefile"] . "</div>\n");
	  print("<div class=\"gridcaption\">" . $row["caption"] . "</div>\n");	  
	  print("</td>\n");

      $intColumnCount++;
	  
	  if ($intColumnCount == $SETPublicGridColumns)
	  {
         print("    </tr>\n");
	     $intColumnCount = 0;	 
      }
   
   }
   
   // finish drawing blank table cells to close off table
   if ($intColumnCount != 0)
   {
     for ($i=$intColumnCount; $i<$SETPublicGridColumns ;$i++) {
         print("<td>&nbsp;</td>\n");
      }
      print("</tr>\n");
   }
   
   print("  </table>\n");
   
   print("</div>\n"); // end imagegrid div
   
}




// ----------------------------------------------------------------------
//   Function    : SqlPrep
//   Description : sets SQL variables to find number of records found
//                 and prepare to run main query
//   Usage       : SqlPrep()
//
// ----------------------------------------------------------------------

function SqlPrep() {

   global $SQLstmt, $SQLselect, $SQLfrom, $SQLwhere;
   global $strKey, $dtMonth, $dtYear, $intCategory;
   global $SETDebug;


   //Build FROM part
   $SQLfrom =  " FROM igimages";

   //Build WHERE part
   $SQLwhere = " WHERE (recordstatus = 'active')";
   

   if ($strKey != "")
   {
      $SQLwhere .= " AND (title LIKE '%$strKey%' OR caption LIKE '%$strKey%'" .
				   " OR keywordtags LIKE '%$strKey%')";
   }
   
   if ($dtMonth != "")
   {
      $SQLwhere .= " AND MONTH(publishdate) = '$dtMonth'";
   }

   if ($dtYear != "")
   {
      $SQLwhere .= " AND YEAR(publishdate) = '$dtYear'";
   }

   if ($intCategory != "")
   {
      $SQLwhere .= " AND categoryID = '$intCategory'";
   }
   
   //SQL to find number of records
   $SQLselect = "SELECT COUNT(*)";
   $SQLstmt = $SQLselect . $SQLfrom . $SQLwhere;


   if ($SETDebug == "ON")
   {
      print("<div class=\"debug\">$SQLstmt</div>");
   }

}




// ----------------------------------------------------------------------
//   Function    : SqlForResults
//   Description : creates the remainder of the sql parts required and 
//                 sets the actual record retrieval SQL query 
//   Usage       : SqlForResults()
//
// ----------------------------------------------------------------------

function SqlForResults() {

   global $SQLstmt, $SQLselect, $SQLfrom, $SQLwhere, $SQLorder;
   global $intStartRecord;
   global $intPublicPageSize;
   global $SETDebug;

   //SQL to retrieve all or a subset of records
   $SQLselect = "SELECT *";
   $SQLorder = " ORDER BY publishdate DESC, title ASC";
   $SQLlimit = " LIMIT $intStartRecord, $intPublicPageSize";
   $SQLstmt = $SQLselect . $SQLfrom . $SQLwhere . $SQLorder . $SQLlimit;

   if ($SETDebug == "ON")
   {
      print("<div class=\"debug\">$SQLstmt</div>\n");
   }

}




// ----------------------------------------------------------------------
//   Function    : SearchFeedback
//   Description : prints feedback on search parameters
//   Usage       : SearchFeedback()
//
// ----------------------------------------------------------------------

function SearchFeedback() {

   global $strKey, $dtMonth, $dtYear, $intCategory;

   print("<div id=\"searchfeedback\">\n");

   if ($strKey != "" || $dtMonth != "" || $dtYear !="" || $intCategory != "")
   {

      print("You searched on&nbsp;&nbsp;");

	  if ($dtMonth != "")
	  {
	     print("Month:&nbsp;<span class=\"searchfeedbackbold\">");
		 
		 switch($dtMonth)
		 {
		    case "1":
			   print("January");
			   break;
			   
			case "2":
			   print("February");
			   break;
			   
			case "3":
			   print("March");
			   break;
			   
			case "4":
			   print("April");
			   break;

			case "5":
			   print("May");
			   break;

			case "6":
			   print("June");
			   break;

			case "7":
			   print("July");
			   break;

			case "8":
			   print("August");
			   break;

			case "9":
			   print("September");
			   break;

			case "10":
			   print("October");
			   break;

			case "11":
			   print("November");
			   break;

			case "12":
			   print("December");
			   break;			   
			   
		 }
		 
		 print("</span> &nbsp;&nbsp;");
	  }
	  
	  if ($dtYear != "")
	  {
	     print("Year: <span class=\"searchfeedbackbold\">$dtYear</span> &nbsp&nbsp;");
	  }
	  
      if ($strKey != "")
	  {
         print("Keyword: <span class=\"searchfeedbackbold\">" . stripslashes($strKey) . "</span> &nbsp;&nbsp;");   		 
      } 	 
   
      if ($intCategory != "")
	  {
	     $SQLstmt2 = "SELECT categoryname FROM igcategory WHERE categoryID = '$intCategory'";
		 $resultSet2 = dbaction($SQLstmt2);
		 $row2 = getrsrow($resultSet2);
		 print("Category: <span class=\"searchfeedbackbold\">" . $row2["categoryname"] . "</span>");
		 
	  }
   
   }

   print("</div>\n"); // end searchfeedback div

}




// ----------------------------------------------------------------------
//   Function    : ResultsStatus
//   Description : shows status info on the number of records returned in
//                 a result list and which ones are currently shown
//   Usage       : ResultsStatus()
//
// ----------------------------------------------------------------------

function ResultsStatus() {

   global $intRecordsFound;
   global $intPage;
   global $intStartRecord;
   global $intPublicPageSize;

   print("<div id=\"recordsfound\">");
   print("Showing results <span class=\"recordsfoundbold\">" . ($intStartRecord+1) . ""); 
   print("</span> - <span class=\"recordsfoundbold\">");

   if (($intPublicPageSize*$intPage) < $intRecordsFound)
      print($intPublicPageSize*$intPage);
   else
      print($intRecordsFound);            

   print(" </span> of <span class=\"recordsfoundbold\">$intRecordsFound</span>");
   print("</div>\n");

}




// ----------------------------------------------------------------------
//   Function    : NoRecordsFound
//   Description : print no records found message
//   Usage       : NoRecordsFound()
//
// ----------------------------------------------------------------------

function NoRecordsFound() {

   print("<div id=\"norecordsfound\">");
   print("Sorry! Your search ");
   print("returned no results at this time.");
   print("</div>\n");

}




// ----------------------------------------------------------------------
//   Function    : RecordCount
//   Description : returns number of records found during search
//   Usage       : RecordCount($SQLstmt)
//   Parameters  : $SQLstmt - SQL for total query record count
//   Returns     : $intRecordsFound - how many records will be returned
//                 by query
//
// ----------------------------------------------------------------------

function RecordCount($SQLstmt) {

   $resultSet = dbaction($SQLstmt);
   $row = getrsrow($resultSet);
   $intRecordsFound = $row[0];
   return $intRecordsFound;

}




// ----------------------------------------------------------------------
//   Function    : GetPageTotal
//   Description : return the number of record result pages available
//                 using the total number of records found and the
//                 page size from the settings.php file
//   Usage       : GetPageTotal()
//   Returns     : intTotalPages - how many record result pages there are
//                 given the number of records found and the page size
//                 setting
//
// ----------------------------------------------------------------------

function GetPageTotal() {

   global $intRecordsFound;
   global $intPublicPageSize;

   if (fmod($intRecordsFound, $intPublicPageSize) == 0)
   {
      $intTotalPages = $intRecordsFound/$intPublicPageSize;      
   }
   else
   {
      $intTotalPages = ceil($intRecordsFound/$intPublicPageSize);
   }

   return $intTotalPages;

}




// ----------------------------------------------------------------------
//   Function    : DisplayPagedNavigation
//   Description : diplays the page navigation links when a search 
//                 result has more than 1 page of records
//   Usage       : DisplayPagedNavigation()
//
// ----------------------------------------------------------------------

function DisplayPagedNavigation() {

   global $intTotalPages;
   global $intPage;
   global $intPublicChapterSize;

   global $strNavPublicState;

   // only show page navigation links if number of pages > 1
   if ($intTotalPages > 1)
   {


      // calculate current chapter based on current page
      if (fmod($intPage, $intPublicChapterSize) == 0)
         $intChapter = ($intPage/$intPublicChapterSize);
      else
         $intChapter = ceil($intPage/$intPublicChapterSize); 


      // display paged record navigation bar
      print("<div id=\"pagenavcontainer\">\n"); 
      print("<div id=\"pagenavigation\">\n");


      // display previous chapter link if it exists
      if ($intChapter > 1)
      {
         print("<div id=\"pagenavprev\">"); 
         print("<a href=\"" . $_SERVER['PHP_SELF'] . "?search=true$strNavPublicState");
         print("&pn=" . ((($intChapter-1)*$intPublicChapterSize)-$intPublicChapterSize+1) . "");
         print("\">Prev $intPublicChapterSize pages</a>");
         print("</div>\n");
      }


      // display page links
      $intStartAt = (($intChapter*$intPublicChapterSize) - ($intPublicChapterSize - 1));

      if ($intTotalPages < ($intChapter*$intPublicChapterSize))
         $intFinishAt = $intTotalPages;
      else
         $intFinishAt = ($intChapter*$intPublicChapterSize);

      for($x=$intStartAt;$x<=$intFinishAt;$x++)
      {
         if ($intPage == $x)
         {
            print("<div class=\"pagenavbox\">$x</div>\n");
         }
         else 
         {
            print("<div class=\"pagenavbox\">");
            print("<a href=\"" . $_SERVER['PHP_SELF'] . "?search=true$strNavPublicState&pn=$x");
            print("\">$x</a></div>\n");
         }
      }

        
      // display next chapter link if it exists
      if (($intChapter * $intPublicChapterSize) < $intTotalPages)
      {
         print("<div id=\"pagenavnext\">");
         print("<a href=\"" . $_SERVER['PHP_SELF'] . "?search=true$strNavPublicState");
         print("&pn=" . (($intChapter*$intPublicChapterSize)+1) . "");
         print("\">Next $intPublicChapterSize pages</a>");
         print("</div>\n");
      }


      print("</div>\n"); // end pagenavigation div
      print("</div>\n"); // end pagenavcontainer div

   } // end if total pages > 0


}




// ----------------------------------------------------------------------
//   Function    : GetPublicState
//   Description : return part of a querystring which can be added to
//                 links to maintain search state
//   Usage       : GetPublicState($intMode)
//   Parameters  : $intMode -  identifies whether to return querystring
//                             components for general or page navigation
//                             links
//
// ----------------------------------------------------------------------

function GetPublicState($intMode) {


   $strPublicState = "";
   $strNavPublicState = "";

   
   if (isset($_REQUEST['key']))
   {
      if ($_REQUEST['key'] != "")
      { 
         $strPublicState .= "&key=" . urlencode($_REQUEST['key']);
      }
   }

   if (isset($_REQUEST['datemonth']))
   {
      if ($_REQUEST['datemonth'] != "")
      { 
         $strPublicState .= "&datemonth=" . urlencode($_REQUEST['datemonth']);
      }
   }
   
   if (isset($_REQUEST['dateyear']))
   {
      if ($_REQUEST['dateyear'] != "")
      { 
         $strPublicState .= "&dateyear=" . urlencode($_REQUEST['dateyear']);
      }
   }
   
   if (isset($_REQUEST['category']))
   {
      if ($_REQUEST['category'] != "")
      { 
         $strPublicState .= "&category=" . urlencode($_REQUEST['category']);
      }
   }

   
   $strNavPublicState = $strPublicState;


   if (isset($_REQUEST['pn']))
   {
      $strPublicState .= "&pn=" . $_REQUEST['pn'];
   }


   //return result based on $intMode supplied
   if ($intMode == 0)
   {
      //pass back querystring version of current state
      return $strPublicState;
   }
   else if ($intMode == 1)
   {
      //pass back querystring version of current state for navigation links
      return $strNavPublicState;  
   }

}




?>

<?php

// ----------------------------------------------------------------------
//   File        : inc_newsaveuser.php
//   Description : displays form to enter a new user record. Submitting
//                 form validates data and saves user record.
//	 			   It is only available to Super and Administrator users.
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("New user");




if ($_SESSION['AccessLevel'] == 'Super' || $_SESSION['AccessLevel'] == 'Administrator')
{

   //get subaction
   if (isset($_REQUEST['subaction']))
      $strSubAction = $_REQUEST['subaction'];
   else
      $strSubAction = NULL;
   
   
   if ($strSubAction == "submit") {
   
      //accept data
	  if (isset($_REQUEST['username']))
         $strUsername = substr($_REQUEST['username'],0,50);
      else
         $strUsername = NULL;
	  
	  if (isset($_REQUEST['password']))
         $strPassword = substr($_REQUEST['password'],0,50);
      else
         $strPassword = NULL;	  
	  
	  if (isset($_REQUEST['userfullname']))
         $strUserFullName = substr($_REQUEST['userfullname'],0,50);
      else
         $strUserFullName = NULL;

	  if (isset($_REQUEST['accesslevel']))
         $strAccessLevel = $_REQUEST['accesslevel'];
      else
         $strAccessLevel = NULL;

	  if (isset($_REQUEST['userstatus']))
         $strUserStatus = $_REQUEST['userstatus'];
      else
         $strUserStatus = NULL;
		 
		 
	  //prepare data if magic quotes is off
      if (!get_magic_quotes_gpc()) {
	     $strUsername = addslashes($strUsername);
		 $strPassword = addslashes($strPassword);
		 $strUserFullName = addslashes($strUserFullName);
		 $strAccessLevel = addslashes($strAccessLevel);
		 $strUserStatus = addslashes($strUserStatus);
	  }
	  
	  
      //validate form
      $booValid = TRUE;
	  $strValidateError = "";

	  //check username field not empty
	  if ($strUsername == "") {
	     $booValid = FALSE;
		 $strValidateError = $strValidateError . "<li>Username cannot be empty<br>";
	  }
	  
	  //check userpassword field not empty
	  if ($strPassword == "") {
	     $booValid = FALSE;
		 $strValidateError = $strValidateError . "<li>Password cannot be empty<br>";
	  }
	  
	  //check userfullname field not empty
	  if ($strUserFullName == "") {
	     $booValid = FALSE;
		 $strValidateError = $strValidateError . "<li>User full name cannot be empty<br>";
	  }
	  	  
	  //check not duplicate username
	  if ($strUsername != "") {
	     $sqlStmt = "SELECT * FROM iguser WHERE username = '$strUsername'";
	     $rs = dbaction($sqlStmt);
	     if ($row = getrsrow($rs)) {
	        $booValid = FALSE;
            $strValidateError = $strValidateError . "<li>Username already exists<br>";		 
	     }
	  }
	  
	  
	  //populate data
	  if ($booValid) {
	     SaveNewUser($strUsername, $strPassword, $strUserFullName, $strAccessLevel, $strUserStatus);
	  }
	  else
	  {
	     DisplayNewUserForm($strValidateError, $strUsername, $strPassword, $strUserFullName, $strAccessLevel, $strUserStatus);
	  }
	  
   }
   else {
      DisplayNewUserForm("", "", "", "", "", "");
   }

}
else
{
   BasicMessage("You do not have access to this area");
}




function DisplayNewUserForm($strValidateError, $strUsername, $strPassword, $strUserFullName, $strAccessLevel, $strUserStatus) {

   global $strFormAdminState;
   
   print("<table class=\"formtable\" width=\"572\" border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\n");
   print("<form action=\"" . $_SERVER['PHP_SELF'] . "\" method=\"post\">\n");
   print("<input type=\"hidden\" name=\"pageaction\" value=\"saveuser\">\n");
   print $strFormAdminState;
   print("<input type=\"hidden\" name=\"subaction\" value=\"submit\">\n"); 
   
   //print errors if submit not valid
   if ($strValidateError != "" ) {
      print("  <tr>\n");
      print("    <td colspan=\"2\"><div id=\"formerror\">ERROR: Form not submitted due to invalid fields<br>" . $strValidateError . "</div></td>\n");
      print("  </tr>\n");
   }
   
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;Username</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"username\" class=\"formitem\"");
   if ($strValidateError != "" )
      print(" value=\"" . stripslashes($strUsername) . "\"");
   print("></td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;Password</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"password\" class=\"formitem\"");
   if ($strValidateError != "" )
      print(" value=\"" . stripslashes($strPassword) . "\"");   
   print("></td>\n");
   print("  </tr>\n"); 
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;User Full Name</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"userfullname\" size=\"40\" class=\"formitem\"");
   if ($strValidateError != "" )
      print(" value=\"" . stripslashes($strUserFullName) . "\"");  
   print("></td>\n");
   print("  </tr>\n");  
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;Access Level</td>\n");
   print("    <td class=\"formtext\"><select name=\"accesslevel\" class=\"formitem\">\n");
   print("        <option value=\"User\"");
   if ($strValidateError != "" )
   {
      if ($strAccessLevel == "User")
	     print(" selected");
   }
   print(">User\n");
   // Super users can create all user types, Administrator users are limited to creating the User type
   if ($_SESSION['AccessLevel'] == "Super")
   {
      print("        <option value=\"Administrator\"");
	  if ($strValidateError != "" )
      {
         if ($strAccessLevel == "Administrator")
	        print(" selected");
      }
	  print(">Administrator\n");
      print("        <option value=\"Super\"");
	  if ($strValidateError != "" )
      {
         if ($strAccessLevel == "Super")
	        print(" selected");
      }
	  print(">Super\n");
   }
   print("</select>\n");
   print("    </td>\n");    
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;Active</td>\n");
   print("    <td class=\"formtext\"><input type=\"checkbox\" name=\"userstatus\" value=\"active\"");
   
   if ($strValidateError != "" )
   {
      if ($strUserStatus == "active")
         print(" checked");    
   }
   else
   {
      print(" checked");
   }
   
   print("></td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formspacer\" colspan=\"2\"></td>\n");
   print("  </tr>\n");     
   print("  <tr>\n");
   print("    <td>&nbsp;</td>\n");
   print("    <td><input type=\"submit\" value=\"save user\"></td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("</form>\n");
   
}




function SaveNewUser($strUsername, $strPassword, $strUserFullName, $strAccessLevel, $strUserStatus) {

   global $strAdminState;

   @ $dtDatetime = date("h:i A l F dS, Y");

   //generate sql statement
   $sqlStmt = "INSERT INTO iguser VALUES (" .
   "NULL, '$strUsername', '$strPassword', '$strUserFullName', '$strAccessLevel', '$strUserStatus', " .
   "'$dtDatetime', '" . addslashes($_SESSION['Username']) . "', '$dtDatetime', '" . addslashes($_SESSION['Username']) . "')";

   // execute statement
   dbaction($sqlStmt); 

   ConfirmMessage("New user record saved", "" . $_SERVER['PHP_SELF'] . "?pageaction=userlist$strAdminState");
  	  
}




?>
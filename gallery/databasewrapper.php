<?php

// ----------------------------------------------------------------------
//   File        : databasewrapper.php
//   Description : All database actions in the application use these
//                 functions.  Currently written for mySQL you could
//                 potentially change them to allow the application to
//                 run on other databases
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




// ----------------------------------------------------------------------
//   Function    : dbaction()
//   Description : Opens a database connection, runs a valid SQL query,
//                 closes the connection, and returns the results
//   Usage       : dbaction($tempQuery)
//   Arguments   : $tempQuery - valid SQL statement
//   Returns     : $dbResult  - query result set
//
// ----------------------------------------------------------------------

function dbaction($tempQuery) {

   global $MySQLservername, $MySQLusername, $MySQLpassword, $MySQLdatabasename, $db;

   $db = @mysqli_connect($MySQLservername, $MySQLusername, $MySQLpassword);
   
   if (mysqli_connect_errno()) {
      print("Database connection failed: " . mysqli_connect_error() . "\n");
      exit();
   }
   
   @mysqli_select_db($db, $MySQLdatabasename)
      or dberrormessage();
   $dbResult = @mysqli_query($db, $tempQuery)
      or dberrormessage();
   mysqli_close($db);
   return $dbResult;

}




// ----------------------------------------------------------------------
//   Function    : dbactiongetid()
//   Description : Opens a database connection, runs a valid SQL query,
//                 closes the connection, and returns the id of the 
//                 created record
//   Usage       : dbactiongetid($tempQuery)
//   Arguments   : $tempQuery - valid SQL statement
//   Returns     : $intNewID
//
// ----------------------------------------------------------------------

function dbactiongetid($tempQuery) {

   global $MySQLservername, $MySQLusername, $MySQLpassword, $MySQLdatabasename;

   $db = @mysqli_connect($MySQLservername, $MySQLusername, $MySQLpassword);

   if (mysqli_connect_errno()) {
      print("Database connection failed: " . mysqli_connect_error() . "\n");
      exit();
   }
   
   @mysqli_select_db($MySQLdatabasename, $db)
     or dberrormessage();
   $dbResult = @mysqli_query($tempQuery, $db)
      or dberrormessage();
   $intNewID = mysqli_insert_id();
   mysqli_close($db);
   return $intNewID;

}




// ----------------------------------------------------------------------
//   Function    : dberrormessage()
//   Description : Prints the first database error and exits application
//   Usage       : dberrormessage()
//
// ----------------------------------------------------------------------

function dberrormessage() {

   global $db;

   $strErrorMessage = "<br>Error: " . mysqli_error($db) ."<br>Error Number: " . mysqli_errno($db);
   print("<br>\n");
   print $strErrorMessage;
   die();   // prevents cascading multiple database errors

}




// ----------------------------------------------------------------------
//   Function    : getrsrow()
//   Description : Fetch the next row of data from a result set
//   Usage       : getrsrow($resultset)
//   Arguments   : $resultset - a result set returned by the dbaction 
//                 function
//   Returns     : $row  - next row of data
//
// ----------------------------------------------------------------------

function getrsrow($resultset) {
   $row = mysqli_fetch_array($resultset);
   return $row;
}




// ----------------------------------------------------------------------
//   Function    : lastrec()
//   Description : Checks to see if a paged record list contains only 1
//                 record.  Used to prepare for state change if the last
//                 record is deleted from a paged list
//   Usage       : getrsrow($resultset)
//   Arguments   : $resultset - a result set returned by the dbaction 
//                 function
//   Returns     : TRUE or nothing 
//
// ----------------------------------------------------------------------

function lastrec($resultset) {
   if (mysqli_num_rows($resultset) == 1)
      return TRUE;
}




?>
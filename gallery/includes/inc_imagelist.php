<?php

// ----------------------------------------------------------------------
//   File        : inc_imagelist.php
//   Description : display a grid of image records
//   Version     : 1.0
//   Created     : 1/3/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




//define global variables
global $intPage;
global $intImagesDisplaySize;
global $intRecordsFound;
global $SQLstmt;
global $intStartRecord;
global $intTotalPages;
global $strAdminState;



//accept variables

if (isset($_REQUEST['pn']))
   $intPage = $_REQUEST['pn'];
else
   $intPage = 1;

$intStartRecord = (($intPage*$intImagesDisplaySize)-($intImagesDisplaySize));




print("<div id=\"funcbar\"><div id=\"adduser\"><a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=newimage$strAdminState\"><img src=\"images/addimage.gif\" border=\"0\" alt=\"add image\"></a></div></div>");

ScreenHeading("Images");




//main list page logic

print("<div id=\"imagegridcontainer\">\n");

DisplaySearchForm();
SqlPrep();
RecordCount();
GetPageTotal();

if ($intRecordsFound > 0)
{
   SqlForResults();
   SearchFeedback();

   print("<div id=\"statusbar\">\n");
   ResultsStatus();
   print("</div>\n"); // end statusbar div

   DisplayRecords();
   DisplayPagedNavigation($intTotalPages, $intPage);
   print("<div id=\"bottomspacer\"></div>");
}
else
{
   SearchFeedback();
   NoRecordsFound();
   print("<div id=\"bottomspacer\"></div>");
}

print("</div>\n"); // end imagegridcontainer div




// ----------------------------------------------------------------------
//   Function    : DisplaySearchForm
//   Description : display a search form including keyword search
//   Usage       : DisplaySearchForm()
//
// ----------------------------------------------------------------------

function DisplaySearchForm() {

   print("<div id=\"searchbar\">\n");
   print("<table>\n");
   print("<form action=\"" . $_SERVER['PHP_SELF'] ."\" method=\"POST\">\n");
   print("<input type=\"hidden\" name=\"pageaction\" value=\"releases\">\n");
   print("  <tr>\n");
   print("    <td class=\"searchfieldshead\">Date</td>\n");
   print("    <td></td>\n");
   print("    <td class=\"searchfieldshead\">Keyword</td>\n");
   print("    <td class=\"searchfieldshead\">Category</td>\n");
   print("    <td></td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td>\n");
   include("includes/monthpartsearch.php");
   print("    </td>\n");
   print("    <td>\n");
   include("includes/yearpartsearch.php");   
   print("    </td>\n");
   print("    <td><input type=\"text\" name=\"key\"></td>\n");
   print("    <td>\n");
   include("includes/categorydropdownsearch.php");
   print("    </td>\n");
   print("    <td><input type=\"image\" src=\"images/search.gif\" class=\"searchbutton\"></td>\n");
   print("  </tr>\n");
   print("</form>\n");
   print("</table>\n");
   print("</div>\n"); // end searchbar div

}




// ----------------------------------------------------------------------
//   Function    : SqlPrep
//   Description : sets SQL variables to find number of records found
//                 and prepare to run main query
//   Usage       : SqlPrep()
//
// ----------------------------------------------------------------------

function SqlPrep() {

   global $SQLstmt, $SQLselect, $SQLfrom, $SQLwhere;
   global $strKey, $dtMonth, $dtYear, $intCategory;
   global $SETDebug;

   //Build FROM part
   $SQLfrom =  " FROM igimages";


   //Build WHERE part
   if ($strKey != "")
   {
      $SQLwhere .= " WHERE (title LIKE '%$strKey%' OR caption LIKE '%$strKey%' or keywordtags LIKE '%$strKey%'" .
	  "or photographer LIKE '%$strKey%')"; 
   }

   if ($dtMonth != "")
   {
      if ($SQLwhere != "")
	  {
	     $SQLwhere .= " AND";
	  }
	  else
	  {
	     $SQLwhere .= " WHERE";
	  }
      $SQLwhere .= " MONTH(publishdate) = '$dtMonth'";
   }

   if ($dtYear != "")
   {
      if ($SQLwhere != "")
	  {
	     $SQLwhere .= " AND";
	  }
	  else
	  {
	     $SQLwhere .= " WHERE";
	  }   
      $SQLwhere .= " YEAR(publishdate) = '$dtYear'";
   }

   if ($intCategory != "")
   {
      if ($SQLwhere != "")
	  {
	     $SQLwhere .= " AND";
	  }
	  else
	  {
	     $SQLwhere .= " WHERE";
	  }
      $SQLwhere .= " categoryID = '$intCategory'";
   }
   
   
   //SQL to find number of records
   $SQLselect = "SELECT COUNT(*)";
   $SQLstmt = $SQLselect . $SQLfrom . $SQLwhere;

   if ($SETDebug == "ON")
   {
      print("<div class=\"debug\">$SQLstmt</div>");
   }

}




// ----------------------------------------------------------------------
//   Function    : RecordCount
//   Description : returns number of records found during search
//   Usage       : RecordCount($SQLstmt)
//   Parameters  : $SQLstmt - SQL for total query record count
//   Returns     : $intRecordsFound - how many records will be returned
//                 by query
//
// ----------------------------------------------------------------------

function RecordCount() {

   global $SQLstmt;
   global $intRecordsFound;

   $resultSet = dbaction($SQLstmt);
   $row = getrsrow($resultSet);
   $intRecordsFound = $row[0];

}




// ----------------------------------------------------------------------
//   Function    : GetPageTotal
//   Description : return the number of record result pages available
//                 using the total number of records found and the
//                 page size from the settings.php file
//   Usage       : GetPageTotal()
//   Returns     : intTotalPages - how many record result pages there are
//                 given the number of records found and the page size
//                 setting
//
// ----------------------------------------------------------------------

function GetPageTotal() {

   global $intImagesDisplaySize;
   global $intRecordsFound;
   global $intTotalPages;

   if (fmod($intRecordsFound, $intImagesDisplaySize) == 0)
   {
      $intTotalPages = $intRecordsFound/$intImagesDisplaySize;      
   }
   else
   {
      $intTotalPages = ceil($intRecordsFound/$intImagesDisplaySize);
   }

}




// ----------------------------------------------------------------------
//   Function    : SqlForResults
//   Description : creates the remainder of the sql parts required and 
//                 sets the actual record retrieval SQL query 
//   Usage       : SqlForResults()
//
// ----------------------------------------------------------------------

function SqlForResults() {

   global $SQLstmt, $SQLselect, $SQLfrom, $SQLwhere, $SQLorder;
   global $intImagesDisplaySize, $intStartRecord;
   global $SETDebug;

   //SQL to retrieve all or a subset of user records
   $SQLselect = "SELECT imageID, publishdate, title, imagefile, caption, photographer, recordstatus";
   $SQLorder = " ORDER BY publishdate DESC, title ASC";
   $SQLlimit = " LIMIT $intStartRecord, $intImagesDisplaySize";
   $SQLstmt = $SQLselect . $SQLfrom . $SQLwhere . $SQLorder . $SQLlimit;

   if ($SETDebug == "ON")
   {
      print("<div class=\"debug\">$SQLstmt</div>\n");
   }

}




// ----------------------------------------------------------------------
//   Function    : SearchFeedback
//   Description : displays the search criteria you specified for the
//                 record search
//   Usage       : SearchFeedback()
//
// ----------------------------------------------------------------------

function SearchFeedback() {

global $strKey, $dtMonth, $dtYear, $intCategory;

   print("<div id=\"searchfeedback\">\n");

   if ($strKey != "" || $dtMonth != "" || $dtYear !="" || $intCategory != "")
   {

      print("You searched on&nbsp;&nbsp;");

	  if ($dtMonth != "")
	  {
	     print("Month:&nbsp;<span class=\"searchfeedbackbold\">");
		 
		 switch($dtMonth)
		 {
		    case "1":
			   print("January");
			   break;
			   
			case "2":
			   print("February");
			   break;
			   
			case "3":
			   print("March");
			   break;
			   
			case "4":
			   print("April");
			   break;

			case "5":
			   print("May");
			   break;

			case "6":
			   print("June");
			   break;

			case "7":
			   print("July");
			   break;

			case "8":
			   print("August");
			   break;

			case "9":
			   print("September");
			   break;

			case "10":
			   print("October");
			   break;

			case "11":
			   print("November");
			   break;

			case "12":
			   print("December");
			   break;			   
			   
		 }
		 
		 print("</span> &nbsp;&nbsp;");
	  }
	  
	  if ($dtYear != "")
	  {
	     print("Year: <span class=\"searchfeedbackbold\">$dtYear</span> &nbsp&nbsp;");
	  }
	  
      if ($strKey != "")
	  {
         print("Keyword: <span class=\"searchfeedbackbold\">" . stripslashes($strKey) . "</span> &nbsp;&nbsp;");   		 
      } 	 
   
      if ($intCategory != "")
	  {
	     $SQLstmt2 = "SELECT categoryname FROM igcategory WHERE categoryID = '$intCategory'";
		 $resultSet2 = dbaction($SQLstmt2);
		 $row2 = getrsrow($resultSet2);
		 print("Category: <span class=\"searchfeedbackbold\">" . $row2["categoryname"] . "</span>");
		 
	  }
   
   }

   print("</div>\n"); // end searchfeedback div

}




// ----------------------------------------------------------------------
//   Function    : ResultsStatus
//   Description : shows status info on the number of records returned in
//                 a result list and which ones are currently shown
//   Usage       : ResultsStatus()
//
// ----------------------------------------------------------------------

function ResultsStatus() {

   global $intPage;
   global $intImagesDisplaySize;
   global $intStartRecord;
   global $intRecordsFound;

   print("<div id=\"recordsfound\">");
   print("Showing results <span class=\"recordsfoundbold\">" . ($intStartRecord+1) . ""); 
   print("</span> - <span class=\"recordsfoundbold\">");

   if (($intImagesDisplaySize*$intPage) < $intRecordsFound)
   {
      print($intImagesDisplaySize*$intPage);
   }
   else
   {
      print($intRecordsFound);
   }            

   print(" </span> of <span class=\"recordsfoundbold\">$intRecordsFound</span>");
   print("</div>\n");

}




// ----------------------------------------------------------------------
//   Function    : DisplayRecords
//   Description : display the query results in a data table
//   Usage       : DisplayRecords()
//
// ----------------------------------------------------------------------

function DisplayRecords() {

   global $strAdminState;
   global $SQLstmt;
   global $SETThumbnailPath;
   global $SETAdminGridColumns;

   
   $resultSet = dbaction($SQLstmt);
   
 
   //check to see if only 1 record is in this page to avoid state errors with deletions later on
   if (lastrec($resultSet)) 
   {
      $strLastRec = "yes";
   } 
   
   
   print("<div id=\"imagegrid\">\n");
   
   print("  <table id=\"imagegridtable\" cellspacing=\"10\">\n");

   
   $intColumnCount = 0;
   
   
   while ($row = getrsrow($resultSet)) {
   
      if ($intColumnCount == 0)
	  {
         print("    <tr>\n");
	  }
   
      print("<td class=\"gridcell\" valign=\"top\">\n");
	  print("<a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=viewimage&id=" . $row["imageID"] . "$strAdminState\"><img src=\"" . $SETThumbnailPath . $row["imagefile"] . "\" border=\"0\"></a>\n");
	  print("<div class=\"gridactions\">\n");
	  print("<a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=editimage&id=" . $row["imageID"] . "$strAdminState\">edit</a>&nbsp;&nbsp;\n");
	  print("<a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=deleteimage&id=" . $row["imageID"] . "&name=" . urlencode($row["title"]) . "$strAdminState");
	  if (isset($strLastRec))
         print("&lastrec=true");      
      print("\">delete</a>\n");
	  print("</div>\n");
	  print("<div class=\"gridpublishdate\">" . $row["publishdate"] . "</div>\n");
	  print("<div class=\"gridtitle\">" . $row["title"] . "</div>\n");
	  print("<div class=\"gridphotographer\">" . $row["photographer"] . "</div>\n");
	  print("<div class=\"gridfilename\">" . $row["imagefile"] . "</div>\n");
	  print("<div class=\"gridcaption\">" . $row["caption"] . "</div>\n");	  
	  print("</td>\n");

      $intColumnCount++;
	  
	  if ($intColumnCount == $SETAdminGridColumns)
	  {
         print("    </tr>\n");
	     $intColumnCount = 0;	 
      }
   
   }
   
   // finish drawing blank table cells to close off table
   if ($intColumnCount != 0)
   {
     for ($i=$intColumnCount; $i<$SETAdminGridColumns ;$i++) {
         print("<td>&nbsp;</td>\n");
      }
      print("</tr>\n");
   }
   
   print("  </table>\n");
   
   print("</div>\n"); // end imagegrid div 
   
   
}




// ----------------------------------------------------------------------
//   Function    : NoRecordsFound
//   Description : print no records found message
//   Usage       : NoRecordsFound()
//
// ----------------------------------------------------------------------

function NoRecordsFound() {

   BasicMessage("No records found");

}


?>


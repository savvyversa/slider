<?php

// ----------------------------------------------------------------------
//   File        : inc_newsavecategory.php
//   Description : displays form to enter a new category record. 
//				   Submitting form validates data and saves category
//                 record.
//	 			   It is only available to Super and Administrator users.
//   Version     : 1.0
//   Created     : 1/2/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("New category");




if ($_SESSION['AccessLevel'] == 'Super' || $_SESSION['AccessLevel'] == 'Administrator')
{

   //get subaction
   if (isset($_REQUEST['subaction']))
      $strSubAction = $_REQUEST['subaction'];
   else
      $strSubAction = NULL;
   
   
   if ($strSubAction == "submit") {
   
      //accept data
	  if (isset($_REQUEST['categoryname']))
         $strCategoryName = substr($_REQUEST['categoryname'],0,100);
      else
         $strCategoryName = NULL;
	  
		 
		 
	  //prepare data if magic quotes is off
      if (!get_magic_quotes_gpc()) {
	     $strCategoryName = addslashes($strCategoryName);
	  }
	  
	  
      //validate form
      $booValid = TRUE;
	  $strValidateError = "";

	  //check category field not empty
	  if ($strCategoryName == "") {
	     $booValid = FALSE;
		 $strValidateError = $strValidateError . "<li>Category name cannot be empty<br>";
	  }
	  	  
	  //check not duplicate categoryname
	  if ($strCategoryName != "") {
	     $sqlStmt = "SELECT * FROM igcategory WHERE categoryname = '$strCategoryName'";
	     $rs = dbaction($sqlStmt);
	     if ($row = getrsrow($rs)) {
	        $booValid = FALSE;
            $strValidateError = $strValidateError . "<li>Category Name already exists<br>";		 
	     }
	  }
	  
	  
	  //populate data
	  if ($booValid) {
	     SaveNewCategory($strCategoryName);
	  }
	  else
	  {
	     DisplayNewCategoryForm($strValidateError, $strCategoryName);
	  }
	  
   }
   else {
      DisplayNewCategoryForm("", "");
   }

}
else
{
   BasicMessage("You do not have access to this area");
}




function DisplayNewCategoryForm($strValidateError, $strCategoryName) {

   global $strFormAdminState;
   
   print("<table class=\"formtable\" width=\"572\" border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\n");
   print("<form action=\"" . $_SERVER['PHP_SELF'] . "\" method=\"post\">\n");
   print("<input type=\"hidden\" name=\"pageaction\" value=\"savecategory\">\n");
   print $strFormAdminState;
   print("<input type=\"hidden\" name=\"subaction\" value=\"submit\">\n"); 
   
   //print errors if submit not valid
   if ($strValidateError != "" ) {
      print("  <tr>\n");
      print("    <td colspan=\"2\"><div id=\"formerror\">ERROR: Form not submitted due to invalid fields<br>" . $strValidateError . "</div></td>\n");
      print("  </tr>\n");
   }
   
   print("  <tr>\n");
   print("    <td class=\"formhead\">&nbsp;Category Name</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"categoryname\" class=\"formitem\"");
   if ($strValidateError != "" )
      print(" value=\"" . $strCategoryName . "\"");
   print("></td>\n");
   print("  </tr>\n");
   
   print("  <tr>\n");
   print("    <td class=\"formspacer\" colspan=\"2\"></td>\n");
   print("  </tr>\n");     
   print("  <tr>\n");
   print("    <td>&nbsp;</td>\n");
   print("    <td><input type=\"submit\" value=\"save category\"></td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("</form>\n");
   
}




function SaveNewCategory($strCategoryName) {

   global $strAdminState;

   @ $dtDatetime = date("h:i A l F dS, Y");

   //generate sql statement
   $sqlStmt = "INSERT INTO igcategory VALUES (" .
   "NULL, '$strCategoryName', '$dtDatetime', '" . addslashes($_SESSION['Username']) . "', '$dtDatetime', '" . addslashes($_SESSION['Username']) . "')";

   // execute statement
   dbaction($sqlStmt); 

   ConfirmMessage("New category record saved", "" . $_SERVER['PHP_SELF'] . "?pageaction=categorylist$strAdminState");
  	  
}




?>
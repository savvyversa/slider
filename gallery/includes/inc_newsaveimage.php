<?php

// ----------------------------------------------------------------------
//   File        : inc_newsaveimage.php
//   Description : displays form to enter a new image. 
//				   Submitting form saves the image record, and uploads
//                 the image, creating a thumbnail file.
//   Version     : 1.0
//   Created     : 1/3/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




include("inc_security.php");




screenheading("New image");




//get subaction
if (isset($_REQUEST['subaction']))
   $strSubAction = $_REQUEST['subaction'];
else
   $strSubAction = NULL;
   
   
if ($strSubAction == "submit") {
   
   //accept data
   if (isset($_REQUEST['dateday']))
      $strDateDay = $_REQUEST['dateday'];
   else
      $strDateDay = "";

   if ($strDateDay == "")
   {
      $strDateDay = "00";
   }
   
   if (isset($_REQUEST['datemonthnewedit']))
      $strDateMonth = $_REQUEST['datemonthnewedit'];
   else
      $strDateMonth = "";

   if ($strDateMonth == "")
   {
      $strDateMonth = "00";
   }
   
   if (isset($_REQUEST['dateyearnewedit']))
      $strDateYear = $_REQUEST['dateyearnewedit'];
   else
      $strDateYear = "";	  

   if ($strDateYear == "")
   {
      $strDateYear = "0000";
   }

   if (isset($_REQUEST['title']))
      $strTitle = substr($_REQUEST['title'],0,255);
   else
      $strTitle = NULL;
	  
   if (isset($_REQUEST['caption']))
      $strCaption = substr($_REQUEST['caption'],0,255);
   else
      $strCaption = NULL;
	  
   if (isset($_REQUEST['keywordtags']))
      $strKeywordTags = substr($_REQUEST['keywordtags'],0,255);
   else
      $strKeywordTags = NULL;	  

   if (isset($_REQUEST['photographer']))
      $strPhotographer = substr($_REQUEST['photographer'],0,100);
   else
      $strPhotographer = NULL;	  
	  
   if (isset($_REQUEST['categorynewedit']))
      $intCategory = $_REQUEST['categorynewedit'];
   else
      $intCategory = NULL;
	  
   if ($intCategory == "")
   {
      $intCategory = 0;
   }

   if (isset($_REQUEST['publish']))
      $strPublish = $_REQUEST['publish'];
   else
      $strPublish = NULL;	  
		 
		 
	//prepare data if magic quotes is off
    if (!get_magic_quotes_gpc()) {
	  $strDateDay = addslashes($strDateDay);
	  $strDateMonth = addslashes($strDateMonth);
	  $strDateYear = addslashes($strDateYear);
	  $strTitle = addslashes($strTitle);
	  $strCaption = addslashes($strCaption);
	  $strKeywordTags = addslashes($strKeywordTags);
	  $strPhotographer = addslashes($strPhotographer);	  
	  $intCategory = addslashes($intCategory);
	  $strPublish = addslashes($strPublish);	  
	}
	  
	  
   SaveNewImage($strDateDay, $strDateMonth, $strDateYear, $strTitle, $strCaption, $strKeywordTags, $strPhotographer, $intCategory, $strPublish);
   
}
else
{
   DisplayNewImageForm();
}




function DisplayNewImageForm() {

   global $strFormAdminState;
   global $SETMaxFileSize;
   
   print("<table class=\"formtable\" width=\"572\" border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\n");
   print("<form enctype=\"multipart/form-data\" action=\"" . $_SERVER['PHP_SELF'] . "\" method=\"post\">\n");
   print("<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"$SETMaxFileSize\">\n");
   print("<input type=\"hidden\" name=\"pageaction\" value=\"saveimage\">\n");
   print $strFormAdminState;
   print("<input type=\"hidden\" name=\"subaction\" value=\"submit\">\n"); 
   
   print("  <tr>\n");
   print("    <td class=\"formhead\">Image File</td>\n");
   print("<td class=\"formtext\">\n");
   print("  <input name=\"photoupload\" type=\"file\" size=\"50\"></td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">Publish Date</td>\n");
   print("    <td class=\"formtext\">");
   include("includes/daypart.php");
   include("includes/monthpart.php");
   include("includes/yearpart.php");
   print("</td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">Title</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"title\" size=\"50\" class=\"formitem\"></td>\n");
   print("  </tr>\n");   
   print("  <tr>\n");
   print("    <td class=\"formhead\">Caption</td>\n");
   print("    <td class=\"formtext\"><textarea name=\"caption\" rows=\"4\" cols=\"50\" class=\"formitem\" onkeyup=\"CheckFieldLength(caption, 'charcount1', 'remaining1', 255);\" onkeydown=\"CheckFieldLength(caption, 'charcount1', 'remaining1', 255);\" onmouseout=\"CheckFieldLength(caption, 'charcount1', 'remaining1',255);\" onmouseover=\"CheckFieldLength(caption, 'charcount1', 'remaining1',255);\"></textarea><br>\n");
   print("<small><span id=\"charcount1\">0</span> characters entered.   |   <span id=\"remaining1\">255</span> characters remaining.</small>\n");
   print("    </td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">Keyword Tags</td>\n");
   print("    <td class=\"formtext\"><textarea name=\"keywordtags\" rows=\"4\" cols=\"50\" class=\"formitem\" onkeyup=\"CheckFieldLength(keywordtags, 'charcount2', 'remaining2', 255);\" onkeydown=\"CheckFieldLength(keywordtags, 'charcount2', 'remaining2', 255);\" onmouseout=\"CheckFieldLength(keywordtags, 'charcount2', 'remaining2',255);\" onmouseover=\"CheckFieldLength(keywordtags, 'charcount2', 'remaining2',255);\"></textarea><br>\n");
   print("<small><span id=\"charcount2\">0</span> characters entered.   |   <span id=\"remaining2\">255</span> characters remaining.</small><br>\n");
   print("(separate keywords with commas)\n");
   print("    </td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"formhead\">Photographer</td>\n");
   print("    <td class=\"formtext\"><input type=\"text\" name=\"photographer\" size=\"50\" class=\"formitem\"></td>\n");
   print("  </tr>\n");   
   print("  <tr>\n");
   print("    <td class=\"formhead\">Category</td>\n");
   print("    <td class=\"formtext\">");
   include("includes/categorydropdown.php");
   print("</td>\n");
   print("  </tr>\n");   
   print("  <tr>\n");
   print("    <td class=\"formhead\">Publish</td>\n");
   print("    <td class=\"formtext\"><input type=\"checkbox\" name=\"publish\" value=\"active\" class=\"formitem\" checked></td>\n");
   print("  </tr>\n");   
   print("  <tr>\n");
   print("    <td class=\"formspacer\" colspan=\"2\"></td>\n");
   print("  </tr>\n");     
   print("  <tr>\n");
   print("    <td>&nbsp;</td>\n");
   print("    <td><input type=\"submit\" value=\"save image\"></td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("</form>\n");
   print("<div id=\"bottomspacer\"></div>");
   
}




function SaveNewImage($strDateDay, $strDateMonth, $strDateYear, $strTitle, $strCaption, $strKeywordTags, $strPhotographer, $intCategory, $strPublish) {

   global $strAdminState;

   include("upload.php");
   
   @ $dtDatetime = date("h:i A l F dS, Y");

   $dtDate = $strDateYear . "-" . $strDateMonth . "-" . $strDateDay;
   
   //generate sql statement
   $sqlStmt = "INSERT INTO igimages VALUES (" .
   "NULL, '$dtDate', '$strTitle', '$UniqueFilename', '$strCaption', '$strKeywordTags', '$strPhotographer', " .
   "'$ImageDimensionx', '$ImageDimensiony', '$intCategory', '$strPublish', " .
   "'$dtDatetime', '" . addslashes($_SESSION['Username']) . "', '$dtDatetime', '" . addslashes($_SESSION['Username']) . "')";
   
   // execute statement
   dbaction($sqlStmt); 

   ConfirmMessage("New image record saved", "" . $_SERVER['PHP_SELF'] . "?pageaction=images$strAdminState");
  	  
}




?>
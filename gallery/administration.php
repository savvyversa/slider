<?php

// ----------------------------------------------------------------------
//   File        : administration.php
//   Description : Image Database administration
//   Version     : 1.0
//   Created     : 1/3/2015
//   Author      : Tim Lockwood
//
// ----------------------------------------------------------------------




//start session
session_start();




//settings and database functions
include("settings.php");
include("databasewrapper.php");




//get an action to perform
if (isset($_REQUEST['pageaction']))
   $PageAction = $_REQUEST['pageaction'];
else
   $PageAction = "";

if (isset($_REQUEST['key']))
   $strKey = addslashes($_REQUEST['key']);
else
   $strKey = "";

if (isset($_REQUEST['datemonth']))
   (int)$dtMonth = $_REQUEST['datemonth'];
else
   $dtMonth = NULL;
   
if (isset($_REQUEST['dateyear']))
   (int)$dtYear = $_REQUEST['dateyear'];
else
   $dtYear = NULL;   

if (isset($_REQUEST['category']))
   (int)$intCategory = $_REQUEST['category'];
else
   $intCategory = NULL;   
   
if (isset($_REQUEST['lastrec']))
   $strLastRec = $_REQUEST['lastrec'];
else
   $strLastRec = "";

   
   

//maintains search state between pages

$strAdminState = GetAdminState(0);
$strNavAdminState = GetAdminState(1);
$strFormAdminState = GetAdminState(2);
$strAdminStateLastRec = GetAdminState(3);



   
//security check determines whether to go to main control, login, or checklogindetails

if (isset($_SESSION['SecurityID']))
{
   if ($_SESSION['SecurityID'] == $SETSecurityID) 
   {
      MainControl();
   }
   else
   {
      Login("");
   }
}
else
{
   if ($PageAction == "checklogindetails")
   {
      CheckLoginDetails();
   }
   else
   {
      if (isset($SETSecurityOverride))
      {
         if ($SETSecurityOverride == "ON")
         {
            SetSecuritySession();
            MainControl();
         }
         else
         {
            Login("");
         }
      }

   }
}




// ----------------------------------------------------------------------
//   Function    : CheckLoginDetails()
//   Description : compares login username and password to database and
//                 logs you in or prints the login form again with an
//                 error message
//   Usage       : CheckLoginDetails()
//
// ----------------------------------------------------------------------

function CheckLoginDetails() {

   global $SETSecurityID;

   //accept clean data
   $strLogin = addslashes($_REQUEST['username']);
   $strPassword = addslashes($_REQUEST['password']);

   //build sql statement
   $sqlStmt =  "SELECT * FROM iguser WHERE username='$strLogin' AND password='$strPassword' AND recordstatus='active'";

   //check database for login details
   $rs = dbaction($sqlStmt);

   if ($row=getrsrow($rs))
   {
      //user detail found
      //set session variables
      $_SESSION['SecurityID']   = $SETSecurityID; //from settings.php
      $_SESSION['Username']     = $row['username'];
      $_SESSION['UserFullName'] = $row['userfullname'];
      $_SESSION['AccessLevel']  = $row['accesslevel'];
      $_SESSION['Area']         = "images";
      maincontrol();
   }
   else 
   {
      //user detail not found display login page with error  
      Login("login details incorrect, please try again!");  
   }

}




// ----------------------------------------------------------------------
//   Function    : SetSecuritySession()
//   Description : sets security related session variables if overriding
//                 security from the settings file
//   Usage       : SetSecuritySession()
//
// ----------------------------------------------------------------------

function SetSecuritySession() {
   global $SETSecurityID;

   $_SESSION['SecurityID']   = $SETSecurityID; // from settings.php
   $_SESSION['Username']     = "override";
   $_SESSION['UserFullName'] = "override";
   $_SESSION['AccessLevel']  = "Super";
   $_SESSION['Area']         = "images";
}




// ----------------------------------------------------------------------
//   Function    : Login()
//   Description : displays a login form with optional error message
//   Usage       : Login($strError)
//   Arguments   : $strError - login error text
//
// ----------------------------------------------------------------------

function Login($strError) {

   PageStart();
   print("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
   print("  <tr>\n");
   print("    <td align=\"center\">\n");
   print("<div id=\"logindiv\">\n");
   print("<div id=\"logintop\"><img src=\"images/loginboxtop.gif\"></div>\n");
   print("<div id=\"logincontent\">\n");
   print("<div id=\"loginheading\">\n");
   print("<div id=\"loginmousey\"><img src=\"images/mousey.gif\"></div>\n");
   print("<div id=\"loginheadingtext\">PHP Image Database</div>\n");
   print("</div>\n"); //end loginheading div
   print("<div id=\"loginspacer\"></div>\n");
   print("<div id=\"loginform\">\n");
   print("<table cellpadding=\"0\" cellspacing=\"8\" border=\"0\">\n");
   print("<form action=\"" . $_SERVER['PHP_SELF'] . "\" method=\"post\">\n");
   print("<input type=\"hidden\" name=\"pageaction\" value=\"checklogindetails\">\n");
   print("  <tr>\n");
   print("    <td class=\"loginformtext\">Username: </td>\n");
   print("    <td><input type=\"text\" name=\"username\" size=\"15\" class=\"loginformitem\"></td>\n");
   print("  </tr>\n");
   print("  <tr>\n");
   print("    <td class=\"loginformtext\">Password: </td>\n");
   print("    <td><input type=\"password\" name=\"password\" size=\"15\" class=\"loginformitem\"></td>\n");
   print("  </tr>\n");
   print("  <tr><td colspan=\"2\" height=\"2\"></td></tr>\n");
   print("  <tr>\n");
   print("    <td colspan=\"2\"><input type=\"image\" src=\"images/login.gif\" value=\"submit\"></td>\n");
   print("  </tr>\n");
   print("</form>\n");
   print("</table>\n");
   print("</div>\n"); //end loginform div
   //If a login error has occured display error message
   if ($strError!="")
   {
      print("<div id=\"loginerror\">$strError</div>\n");
   }    
   print("</div>\n"); //end logincontent div
   print("<div id=\"loginbottom\"><img src=\"images/loginboxbottom.gif\"></div>\n");
   print("</div>\n"); //end login div

   print("    </td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("\n");
   PageEnd();

}




// ----------------------------------------------------------------------
//   Function    : MainControl()
//   Description : constructs each page of the administration interface
//                 dynamically presenting action screens based on the 
//                 pageaction variable
//   Usage       : MainControl()
//
// ----------------------------------------------------------------------

function maincontrol() {

   global $PageAction;
   global $SETDebug, $strAdminState, $strNavAdminState, $strFormAdminState, $strAdminStateLastRec;

   //build the html page to where the action screen starts
   PageStart();

   if ($SETDebug=="ON")
   {
      print("<div class=\"debug\">ADMIN: $strAdminState<br>NAV ADMIN: $strNavAdminState<br><br>$strFormAdminState<br><br>LAST REC: $strAdminStateLastRec</div>\n");
   } 

   StartPageContainer();

   //set area session to allow main menu to show current area
   if ($PageAction == "images" || $PageAction == "" || $PageAction == "checklogindetails")
         $_SESSION['Area'] = "images";
		 
   if ($PageAction == "categorylist")
         $_SESSION['Area'] = "categories";

   if ($PageAction == "userlist")
         $_SESSION['Area'] = "user";

   PageHeader();

   StartPageContent();


   switch($PageAction)
   {

      //releases actions      

      case "images":
         include("includes/inc_imagelist.php");
         break;

      case "viewimage":
         include("includes/inc_viewimage.php");
         break;

      case "newimage":
         include("includes/inc_newsaveimage.php");
         break;

      case "saveimage":
         include("includes/inc_newsaveimage.php");
         break;

      case "editimage":
         include("includes/inc_editupdateimage.php");
         break;
		 
      case "updateimage":
         include("includes/inc_editupdateimage.php");
         break;

      case "deleteimage":
         include("includes/inc_deleteimage.php");
         break;
      
	  //category actions

      case "categorylist":
         include("includes/inc_categorylist.php");
         break;

      case "viewcategory":
         include("includes/inc_viewcategory.php");
         break;

      case "newcategory":
	     include("includes/inc_newsavecategory.php");
         break;

      case "savecategory":
         include("includes/inc_newsavecategory.php");
         break;

      case "editcategory":
         include("includes/inc_editupdatecategory.php");
         break;

      case "updatecategory":
         include("includes/inc_editupdatecategory.php");
         break;

      case "deletecategory":
         include("includes/inc_deletecategory.php");
         break;
		 
      //user actions

      case "userlist":
         include("includes/inc_userlist.php");
         break;

      case "viewuser":
         include("includes/inc_viewuser.php");
         break;

      case "newsaveuser":
         include("includes/inc_newsaveuser.php");
         break;

      case "saveuser":
         include("includes/inc_newsaveuser.php");
         break;

      case "edituser":
         include("includes/inc_editupdateuser.php");
         break;

      case "updateuser":
         include("includes/inc_editupdateuser.php");
         break;

      case "deleteuser":
         include("includes/inc_deleteuser.php");
         break;

      //default action

      default :
         include("includes/inc_imagelist.php");
         break;
   }


   // finish building the html page after where the action screen finishes
   EndPageContent();
   EndPageContainer();
   PageEnd();

}




// ----------------------------------------------------------------------
//   Function    : PageStart()
//   Description : prints the beginning of a HTML page to the browser
//   Usage       : PageStart()
//
// ----------------------------------------------------------------------

function PageStart() {

   global $SETApplicationName;

   print("<!DOCTYPE html>\n");
   print("<html>\n");
   print("<head>\n");
   print("  <title>$SETApplicationName</title>\n");
   print("  <link rel=stylesheet href=\"administration.css\" type=\"text/css\">\n");
   
   //only reference char count javascript for certain screens
   if (isset($_REQUEST['pageaction'])) {
      if ($_REQUEST['pageaction'] == "newimage" || $_REQUEST['pageaction'] == "editimage") {
         print("  <script type=\"text/javascript\" src=\"charcount.js\"></script>\n");
      }
   }
   print("</head>\n");
   print("<body");
   
   if (isset($_REQUEST['pageaction'])) {
      if ($_REQUEST['pageaction'] == "editimage") {
         print(" onload=\"CheckFieldLengthsOnEdit()\"");
	  }
   }
   
   print(">\n");

}




// ----------------------------------------------------------------------
//   Function    : StartPageContainer()
//   Description : prints the start of a division which acts as a 
//                 container for all other pages elements
//   Usage       : StartPageContainer()
//
// ----------------------------------------------------------------------

function StartPageContainer() {

   print("<div id=\"pagecontainer\">\n");

}




// ----------------------------------------------------------------------
//   Function    : PageHeader()
//   Description : prints a page header within the page container
//   Usage       : PageHeader()
//
// ----------------------------------------------------------------------

function PageHeader() {

   global $strAdminState;
   
   print("<div id=\"pageheader\">\n");

   print("<div id=\"pageheaderrow1\">\n");
   print("<div id=\"signout\"><a href=\"signout.php\"><img src=\"images/signout.gif\" alt=\"signout\" border=\"0\"></a></div>\n");
   print("<div id=\"userstatus\">signed in as: " . $_SESSION["Username"] . "</div>\n");
   print("</div>\n"); // end pageheaderrow1 div
   
   print("<div id=\"pageheaderrow2\">\n");   
   print("<div id=\"logo\"><img src=\"images/logo.gif\" alt=\"php image database\"></div>\n");
   print("<div id=\"logomenuspacer\"></div>\n");
   
   print("<div id=\"mainmenu\">\n");
   print("<div id=\"menuoptions\">\n");

   print("<div class=\"menuoption\"><a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=images");
   if ($_SESSION['Area']=="images")
      print $strAdminState;
   print("\">");
   if ($_SESSION['Area']=="images")
      print("<img src=\"images/imagesonbutton.gif\" alt=\"images\" border=\"0\">");
   else
      print("<img src=\"images/imagesoffbutton.gif\" alt=\"images\" border=\"0\">");
   print("</a></div>\n");

   
   //Only let administrator and superusers access the category and user sections.
   //Further security is included in the pages contained in these sections.
   if ($_SESSION['AccessLevel']=="Super" || $_SESSION['AccessLevel']=="Administrator")
   {

	  print("<div class=\"menuoption\"><a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=categorylist");
      if ($_SESSION['Area']=="categories")
         print $strAdminState;
      print("\">");
      if ($_SESSION['Area']=="categories")
         print("<img src=\"images/categoriesonbutton.gif\" alt=\"categories\" border=\"0\">");
      else
         print("<img src=\"images/categoriesoffbutton.gif\" alt=\"categories\" border=\"0\">");
      print("</a></div>\n");


      print("<div class=\"menuoption\"><a href=\"" . $_SERVER['PHP_SELF'] . "?pageaction=userlist");
      if ($_SESSION['Area']=="user")
         print $strAdminState;
      print("\">");
      if ($_SESSION['Area']=="user")
         print("<img src=\"images/usersonbutton.gif\" alt=\"users\" border=\"0\">");
      else
         print("<img src=\"images/usersoffbutton.gif\" alt=\"users\" border=\"0\">");
      print("</a></div>\n");

   }

   print("</div>\n"); // end menu options div
   print("</div>\n"); // end main menu div
   print("</div>\n"); // end pageheaderrow2 div
   
   print("<div id=\"pageheaderrow3\"></div>\n");
   print("</div>\n"); // end pageheader div

}




// ----------------------------------------------------------------------
//   Function    : StartPageContent()
//   Description : prints the start of a division for the main content
//   Usage       : StartPageContent()
//
// ----------------------------------------------------------------------

function StartPageContent() {
   
   print("<div id=\"pagecontent\">\n");

}




// ----------------------------------------------------------------------
//   Function    : EndPageContent()
//   Description : closes page content div
//   Usage       : EndPageContent()
//
// ----------------------------------------------------------------------

function EndPageContent() {

   print("</div>\n");

}




// ----------------------------------------------------------------------
//   Function    : EndPageContainer()
//   Description : prints the end of a division which acts as a
//                 container for all other page elements
//   Usage       : EndPageContainer()
//
// ----------------------------------------------------------------------

function EndPageContainer() {

   print("</div>\n");

}




// ----------------------------------------------------------------------
//   Function    : PageEnd()
//   Description : prints the end of a HTML page to the web browser
//   Usage       : PageEnd()
//
// ----------------------------------------------------------------------

function PageEnd() {

   print("</body>\n");
   print("</html>\n");
   
}




// ----------------------------------------------------------------------
//   Function    : ScreenHeading()
//   Description : prints a heading in the content area
//   Usage       : ScreenHeading($strHeading)
//   Arguments   : $strHeading - text to display in heading
//
// ----------------------------------------------------------------------

function ScreenHeading($strHeading) {

   print("<div id=\"screenheading\">$strHeading</div>\n"); 

}




// ----------------------------------------------------------------------
//   Function    : BasicMessage()
//   Description : prints a message in the content area
//   Usage       : BasicMessage($strMessage)
//   Arguments   : $strMessage - text to display in message
// ----------------------------------------------------------------------

function BasicMessage($strMessage) {

   print("<div id=\"messagecontainer\">");
   print("<table align=\"center\">\n");
   print("  <tr>\n");
   print("    <td align=\"center\">\n");
   print("<div id=\"basicmsgtext\">" . $strMessage . "</div>");
   print("    </td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("</div>\n"); 

}




// ----------------------------------------------------------------------
//   Function    : ConfirmMessage()
//   Description : prints a message in the content area with a continue
//                 link
//   Usage       : ConfirmMessage($strMessage, $strUrl)
//   Arguments   : $strMessage - text to display in message
//               : $strUrl - link for continue button
//
// ----------------------------------------------------------------------

function ConfirmMessage($strMessage, $strUrl) {

   print("<div id=\"messagecontainer\">");
   print("<table align=\"center\">\n");
   print("  <tr>\n");
   print("    <td align=\"center\">\n");
   print("<div id=\"msgtext\">" . $strMessage . "</div>");
   print("<div id=\"msgbuttons\"><a href=\"" . $strUrl . "\"><img src=\"images/continue.gif\" border=\"0\"></a></div>\n");
   print("    </td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("</div>\n"); 

}




// ----------------------------------------------------------------------
//   Function    : ConfirmCancelMessage()
//   Description : prints a message in the content area with a confirm
//                 link and a cancel link
//   Usage       : ConfirmCancelMessage($strMessage, $strUrl1, $strUrl2)
//   Arguments   : $strMessage - text to display in message
//               : $strUrl1 - link for confirm button
//               : #strUrl2 - link for cancel button
//
// ----------------------------------------------------------------------

function ConfirmCancelMessage($strMessage, $strUrl1, $strUrl2) {

   print("<div id=\"messagecontainer\">");
   print("<table align=\"center\">\n");
   print("  <tr>\n");
   print("    <td align=\"center\">\n");
   print("<div id=\"msgtext\">" . $strMessage . "</div>");
   print("<div id=\"msgbuttons\">");
   print("<a href=\"" . $strUrl1 . "\"><img src=\"images/confirm.gif\" border=\"0\"></a>\n");
   print("<a href=\"" . $strUrl2 . "\"><img src=\"images/cancel.gif\" border=\"0\"></a>\n");
   print("</div>\n"); // end div msgbuttons
   print("    </td>\n");
   print("  </tr>\n");
   print("</table>\n");
   print("</div>\n");
   
}




// ----------------------------------------------------------------------
//   Function    : DisplayPagedNavigation
//   Description : diplays the page navigation links when a search 
//                 result has more than 1 page of records
//   Usage       : DisplayPagedNavigation($intTotalPages,$intPage)
//   Arguments   : $intTotalPages -  total number of results found during
//                 the search
//                 $intPage - the current page identifier
//
// ----------------------------------------------------------------------

function DisplayPagedNavigation($intTotalPages,$intPage) {

   global $intChapterSize;   // how many pages per chapter - see settings.php
   global $strNavAdminState;

   // only show page navigation links if number of pages > 1
   if ($intTotalPages > 1)
   {

      // calculate current chapter based on current page
      if (fmod($intPage, $intChapterSize) == 0)
         $intChapter = ($intPage/$intChapterSize);
      else
         $intChapter = ceil($intPage/$intChapterSize); 

      // display paged record navigation bar
      print("<div id=\"pagenavcontainer\">\n"); 
      print("<div id=\"pagenavigation\">\n");

      // display previous chapter link if it exists
      if ($intChapter > 1)
      {
         print("<div id=\"pagenavprev\">"); 
         print("<a href=\"" . $_SERVER['PHP_SELF'] . "$strNavAdminState&pn=" . ((($intChapter-1)*$intChapterSize)-$intChapterSize+1) . "");
         print("\">Prev $intChapterSize pages</a>");
         print("</div>");
      }

      // display page links
      $intStartAt = (($intChapter*$intChapterSize) - ($intChapterSize - 1));
      //print("startat: $intStartat<br>");

      if ($intTotalPages < ($intChapter*$intChapterSize))
         $intFinishAt = $intTotalPages;
      else
         $intFinishAt = ($intChapter*$intChapterSize);
      //print("finishat: $intFinishat<br>");

      for($x=$intStartAt;$x<=$intFinishAt;$x++)
      {
         if ($intPage == $x)
         {
            print("    <div class=\"pagenavbox\">[$x]</div>\n");
         }
         else 
         {
            print("    <div class=\"pagenavbox\"><a href=\"" . $_SERVER['PHP_SELF'] . "$strNavAdminState&pn=$x");
            print("\">$x</a></div>\n");
         }
      }
        
      // display next chapter link if it exists
      if (($intChapter * $intChapterSize) < $intTotalPages) {
         print("<div id=\"pagenavnext\">");
         print("<a href=\"" . $_SERVER['PHP_SELF'] . "$strNavAdminState&pn=" . (($intChapter*$intChapterSize)+1) . "");
         print("\">Next $intChapterSize pages</a>");
         print("</div>");
      }

      print("</div>\n"); // end pagenavigation div
      print("</div>\n"); // end pagenavcontainer div

   } // end if total pages > 0


}




// ----------------------------------------------------------------------
//   Function    : GetAdminState
//   Description : return part of a querystring which can be added to
//                 links to maintain search state, or return the same
//                 information in hidden form elements
//   Usage       : GetAdminState($intMode)
//   Arguments   : $intMode -  identifies whether to return querystring
//                             components for general or page navigation
//                             links, or whether to return hidden form
//                             elements
//
// ----------------------------------------------------------------------

function GetAdminState($intMode) {

   global $SETDebug;
   global $strLastRec;


   $strAdminState = "";
   $strAdminStateLastRec = "";
   $strNavAdminState = "";
   $strFormAdminState = "";


   if (isset($_REQUEST['key']))
   {
      if ($_REQUEST['key'] != "")
      { 
         $strAdminState .= "&key=" . urlencode($_REQUEST['key']);
         $strFormAdminState  .= "<input type=\"hidden\" name=\"key\" value=\"" . urldecode(urlencode($_REQUEST['key'])) ."\">";
      }
   }

   
   if (isset($_REQUEST['datemonth']))
   {
      if ($_REQUEST['datemonth'] != "")
      { 
         $strAdminState .= "&datemonth=" . urlencode($_REQUEST['datemonth']);
         $strFormAdminState  .= "<input type=\"hidden\" name=\"datemonth\" value=\"" . urldecode(urlencode($_REQUEST['datemonth'])) ."\">";
      }
   }
   

   if (isset($_REQUEST['dateyear']))
   {
      if ($_REQUEST['dateyear'] != "")
      { 
         $strAdminState .= "&dateyear=" . urlencode($_REQUEST['dateyear']);
         $strFormAdminState  .= "<input type=\"hidden\" name=\"dateyear\" value=\"" . urldecode(urlencode($_REQUEST['dateyear'])) ."\">";
      }
   }
   
   
   if (isset($_REQUEST['category']))
   {
      if ($_REQUEST['category'] != "")
      { 
         $strAdminState .= "&category=" . urlencode($_REQUEST['category']);
         $strFormAdminState  .= "<input type=\"hidden\" name=\"category\" value=\"" . urldecode(urlencode($_REQUEST['category'])) ."\">";
      }
   }


   if (isset($_REQUEST['pageaction']))
   {
      //changes pageaction variable when you first login to help build page navigation
      if ($_REQUEST['pageaction'] == "checklogindetails")
      {
         $tmppageaction = "releases";
      }
      else
      {
         $tmppageaction = $_REQUEST['pageaction'];
      }

      $strNavAdminState = "?pageaction=" . $tmppageaction . $strAdminState;
   }

   //generate a second admin state variable for use in confirm delete to avoid showing
   //a page which doesn't exist anymore because the last row on that page has just been
   //deleted
   $strAdminStateLastRec = $strAdminState;

   if (isset($_REQUEST['pn']))
   {
      $strAdminState .= "&pn=" . $_REQUEST['pn'];
      if ($strLastRec == "true")
      {
         $tmppn = $_REQUEST['pn'];
         $tmppn--;
         $strAdminStateLastRec .= "&pn=$tmppn";
      }
      else
      {
         $strAdminStateLastRec = $strAdminState;
      }
      $strFormAdminState .= "<input type=\"hidden\" name=\"pn\" value=\"" . $_REQUEST['pn'] . "\">";
   }


   //return result based on $intMode supplied
   if ($intMode == 0)
   {
      //pass back querystring version of current state
      return $strAdminState;
   }
   else if ($intMode == 1)
   {
      //pass back querystring version of current state for navigation links
      return $strNavAdminState;  
   }
   else if ($intMode == 2)
   {
      //pass back hidden form variable version of current state
      return $strFormAdminState;
   }
   else if ($intMode == 3)
   {
      //pass modified admin state string for use when last row on a page is deleted
      return $strAdminStateLastRec;
   }

}




?>